import numpy as np
from openmdao.api import ExplicitComponent
from openmdao.api import Group
import math

##TODO: add fuel system weight back in (depends on Wf, which depends on MTOW and We, and We depends on fuel system weight)

class WingWeight_SmallTurboprop(ExplicitComponent):
    """Inputs: MTOW, S_ref, AR_wing, c4sweep_wing, taper_wing, toverc_wing, V_H (max SL speed)
    Outputs: W_wing
    Metadata: n_ult (ult load factor)

    """
    def initialize(self):
        #self.options.declare('num_nodes', default=1, desc='Number of flight/control conditions')
        #define configuration parameters
        self.options.declare('n_ult', default=3.8*1.5, desc='Ultimate load factor (dimensionless)')


    def setup(self):
        #nn = self.options['num_nodes']
        self.add_input('MTOW', units='lb', desc='Maximum rated takeoff weight')
        self.add_input('W_fuel', units='lb', desc='Fuel weight')
        self.add_input('S_ref', units='ft**2', desc='Reference wing area in sq ft')
        self.add_input('AR_wing', desc='Wing aspect ratio')
        self.add_input('c4sweep_wing', units='rad', desc='Quarter-chord sweep angle')
        self.add_input('taper_wing', desc='Wing taper ratio')
        self.add_input('toverc_wing', desc='Wing max thickness to chord ratio')
        self.add_input('V_H', units='kn', desc='Max sea-level speed')
        self.add_input('q_cruise', units='lb*ft**-2')


        #self.add_output('heat_out', units='W', desc='Waste heat out',shape=(nn,))
        self.add_output('W_wing', units='lb', desc='Wing weight')

        self.declare_partials(['*'], 'W_wing')
            
    def compute(self, inputs, outputs):
        n_ult = self.options['n_ult']
        #USAF method, Roskam PVC5pg68eq5.4
        W_wing_USAF = 96.948*((inputs['MTOW']*n_ult/1e5)**0.65 * (inputs['AR_wing']/math.cos(inputs['c4sweep_wing']))**0.57 * (inputs['S_ref']/100)**0.61 * ((1+inputs['taper_wing'])/2/inputs['toverc_wing'])**0.36 * (1+inputs['V_H']/500)**0.5)**0.993
        #Torenbeek, Roskam PVC5p68eq5.5
        b = math.sqrt(inputs['S_ref']*inputs['AR_wing'])
        root_chord = 2*inputs['S_ref']/b/(1+inputs['taper_wing'])
        tr = root_chord * inputs['toverc_wing']
        c2sweep_wing = inputs['c4sweep_wing'] # a hack for now
        W_wing_Torenbeek = 0.00125*inputs['MTOW'] * (b/math.cos(c2sweep_wing))**0.75 * (1+ (6.3*math.cos(c2sweep_wing)/b)**0.5) * n_ult**0.55 * (b*inputs['S_ref']/tr/inputs['MTOW']/math.cos(c2sweep_wing))**0.30

        W_wing_Raymer = 0.036 * inputs['S_ref']**0.758 * inputs['W_fuel']**0.0035 * (inputs['AR_wing']/math.cos(inputs['c4sweep_wing'])**2)**0.6 * inputs['q_cruise']**0.006 * inputs['taper_wing']**0.04 * (100*inputs['toverc_wing']/math.cos(inputs['c4sweep_wing']))**-0.3 * (n_ult * inputs['MTOW'])**0.49

        outputs['W_wing'] = W_wing_Raymer

    # def compute_partials(self, inputs, J):
    #     eta_b = self.options['efficiency']
    #     p_b = self.options['specific_power']
    #     J['component_sizing_margin','elec_load'] = 1 / (p_b * inputs['battery_weight'])
    #     J['component_sizing_margin','battery_weight'] = - inputs['elec_load'] / (p_b * inputs['battery_weight'] ** 2)

class EmpennageWeight_SmallTurboprop(ExplicitComponent):
    """Inputs: MTOW, S_ref, AR_wing, c4sweep_wing, taper_wing, toverc_wing, V_H (max SL speed)
    Outputs: W_wing
    Metadata: n_ult (ult load factor)

    """
    def initialize(self):
        #self.options.declare('num_nodes', default=1, desc='Number of flight/control conditions')
        #define configuration parameters
        self.options.declare('n_ult', default=3.8*1.5, desc='Ultimate load factor (dimensionless)')


    def setup(self):
        #nn = self.options['num_nodes']
        self.add_input('MTOW', units='lb', desc='Maximum rated takeoff weight')
        self.add_input('S_h', units='ft**2', desc='Projected horiz stab area in sq ft')
        self.add_input('S_v', units='ft**2', desc='Projected vert stab area in sq ft')
        self.add_input('l_h', units='ft', desc='Distance from wing c/4 to horiz stab c/4 (tail arm distance)')

        self.add_input('AR_h', desc='Horiz stab aspect ratio')
        self.add_input('AR_v', units='rad', desc='Vert stab aspect ratio')
        self.add_input('troot_h', units='ft', desc='Horiz stab root thickness (ft)')
        self.add_input('troot_v', units='ft', desc='Vert stab root thickness (ft)')
        #self.add_input('q_cruise', units='lb*ft**-2', desc='Cruise dynamic pressure')

        #self.add_output('heat_out', units='W', desc='Waste heat out',shape=(nn,))
        self.add_output('W_empennage', units='lb', desc='Empennage weight')
        self.declare_partials(['*'], 'W_empennage')
            
    def compute(self, inputs, outputs):
        n_ult = self.options['n_ult']
        #USAF method, Roskam PVC5pg72eq5.14/15
        bh = math.sqrt(inputs['S_h']*inputs['AR_h'])
        bv = math.sqrt(inputs['S_v']*inputs['AR_v'])
        Wh = 127 * ((inputs['MTOW']*n_ult/1e5)**0.87 * (inputs['S_h']/100)**1.2 * 0.289*(inputs['l_h']/10)**0.483 * (bh/inputs['troot_h'])**0.5)**0.458
        #Wh_raymer = 0.016 * (n_ult*inputs['MTOW'])**0.414 * inputs['q_cruise']**0.168 * inputs['S_h']**0.896 * (100 * 0.18)**-0.12 * (inputs['AR_h'])**0.043 * 0.7**-0.02
        Wv = 98.5 * ((inputs['MTOW']*n_ult/1e5)**0.87 * (inputs['S_v']/100)**1.2 * 0.289 * (bv/inputs['troot_v'])**0.5)**0.458
    
        Wemp_USAF = Wh + Wv

        #Torenbeek, Roskam PVC5p73eq5.16
        Wemp_Torenbeek = 0.04 * (n_ult * (inputs['S_v'] + inputs['S_h'])**2)**0.75
        print(Wemp_Torenbeek)


        outputs['W_empennage'] = Wemp_Torenbeek

        

    # def compute_partials(self, inputs, J):
    #     eta_b = self.options['efficiency']
    #     p_b = self.options['specific_power']
    #     J['component_sizing_margin','elec_load'] = 1 / (p_b * inputs['battery_weight'])
    #     J['component_sizing_margin','battery_weight'] = - inputs['elec_load'] / (p_b * inputs['battery_winputseight'] ** 2)

class FuselageWeight_SmallTurboprop(ExplicitComponent):
    def initialize(self):
        #self.options.declare('num_nodes', default=1, desc='Number of flight/control conditions')
        #define configuration parameters
        self.options.declare('n_ult', default=3.8*1.5, desc='Ultimate load factor (dimensionless)')


    def setup(self):
        #nn = self.options['num_nodes']
        self.add_input('MTOW', units='lb', desc='Maximum rated takeoff weight')
        self.add_input('l_f', units='ft', desc='Fuselage length (not counting nacelle')
        self.add_input('h_f', units='ft', desc='Fuselage height')
        self.add_input('w_f', units='ft', desc='Fuselage weidth')
        self.add_input('V_C', units='kn', desc='Indicated cruise airspeed (KEAS)')
        self.add_input('V_MO', units='kn', desc='Max operating speed (indicated)')
        self.add_input('S_f', units='ft**2', desc='Fuselage shell area')
        self.add_input('l_h', units='ft', desc='Horiz tail arm')
        self.add_input('q_cruise', units='lb*ft**-2', desc='Dynamic pressure at cruise')

        #self.add_output('heat_out', units='W', desc='Waste heat out',shape=(nn,))
        self.add_output('W_fuselage', units='lb', desc='Fuselage weight')
        self.declare_partials(['*'], 'W_fuselage')
            
    def compute(self, inputs, outputs):
        n_ult = self.options['n_ult']
        #USAF method, Roskam PVC5pg76eq5.25
        W_fuselage_USAF = 200*((inputs['MTOW']*n_ult/1e5)**0.286 * (inputs['l_f']/10)**0.857 * (inputs['w_f']+inputs['h_f'])/10 * (inputs['V_C']/100)**0.338)**1.1
        print(W_fuselage_USAF)

        W_fuselage_Torenbeek = 0.021 * 1.08 * ((inputs['V_MO']*inputs['l_h']/(inputs['w_f']+inputs['h_f']))**0.5 * inputs['S_f']**1.2)
        W_press = 11.9*(math.pi*(inputs['w_f']+inputs['h_f'])/2*inputs['l_f']*0.8  * 8)**0.271
        W_fuselage_Raymer = 0.052 * inputs['S_f']**1.086 * (n_ult * inputs['MTOW'])**0.177 * inputs['l_h']**-0.051 * (inputs['l_f']/inputs['h_f'])**-0.072 * inputs['q_cruise']**0.241 + W_press
        print(W_press)
        outputs['W_fuselage'] = W_fuselage_Raymer

    # def compute_partials(self, inputs, J):
    #     eta_b = self.options['efficiency']
    #     p_b = self.options['specific_power']
    #     J['component_sizing_margin','elec_load'] = 1 / (p_b * inputs['battery_weight'])
    #     J['component_sizing_margin','battery_weight'] = - inputs['elec_load'] / (p_b * inputs['battery_weight'] ** 2)

class NacelleWeight_SmallTurboprop(ExplicitComponent):
    """Inputs: MTOW, S_ref, AR_wing, c4sweep_wing, taper_wing, toverc_wing, V_H (max SL speed)
    Outputs: W_wing
    Metadata: n_ult (ult load factor)

    """
    def initialize(self):
        #self.options.declare('num_nodes', default=1, desc='Number of flight/control conditions')
        #define configuration parameters
        self.options.declare('n_ult', default=3.8*1.5, desc='Ultimate load factor (dimensionless)')


    def setup(self):
        #nn = self.options['num_nodes']
        self.add_input('P_TO', units='hp', desc='Takeoff power')

        #self.add_output('heat_out', units='W', desc='Waste heat out',shape=(nn,))
        self.add_output('W_nacelle', units='lb', desc='Nacelle weight')
        self.declare_partials(['*'], 'W_nacelle')
            
    def compute(self, inputs, outputs):
        n_ult = self.options['n_ult']
        #Torenbeek method, Roskam PVC5pg78eq5.30
        W_nacelle = 2.5*inputs['P_TO']**0.5
        print(W_nacelle)
        outputs['W_nacelle'] = W_nacelle

class LandingGearWeight_SmallTurboprop(ExplicitComponent):
    """Inputs: MTOW, S_ref, AR_wing, c4sweep_wing, taper_wing, toverc_wing, V_H (max SL speed)
    Outputs: W_wing
    Metadata: n_ult (ult load factor)

    """
    def initialize(self):
        #self.options.declare('num_nodes', default=1, desc='Number of flight/control conditions')
        #define configuration parameters
        self.options.declare('n_ult', default=3.8*1.5, desc='Ultimate load factor (dimensionless)')


    def setup(self):
        #nn = self.options['num_nodes']
        self.add_input('MTOW',units='lb',desc='Max takeoff weight')
        self.add_input('MLW', units='lb', desc='Max landing weight')
        self.add_input('l_mg', units='ft', desc='Main landing gear extended length')
        self.add_input('l_ng', units='ft', desc='Nose gear extended length')

        #self.add_output('heat_out', units='W', desc='Waste heat out',shape=(nn,))
        self.add_output('W_gear', units='lb', desc='Gear weight (nose and main)')
        self.declare_partials(['*'], 'W_gear')
            
    def compute(self, inputs, outputs):
        n_ult = self.options['n_ult']
        #Torenbeek method, Roskam PVC5pg82eq5.42
        W_gear_Torenbeek_main = 33.0+0.04*inputs['MTOW']**0.75 + 0.021*inputs['MTOW']
        W_gear_Torenbeek_nose = 12.0+0.06*inputs['MTOW']**0.75

        W_gear_Raymer_main = 0.095*(n_ult*inputs['MLW'])**0.768 * (inputs['l_mg']/12)**0.409
        W_gear_Raymer_nose = 0.125*(n_ult*inputs['MLW'])**0.566 * (inputs['l_ng']/12)**0.845


        W_gear = W_gear_Raymer_main + W_gear_Raymer_main
        outputs['W_gear'] = W_gear


class FuelSystemWeight_SmallTurboprop(ExplicitComponent):
    def initialize(self):
        #self.options.declare('num_nodes', default=1, desc='Number of flight/control conditions')
        #define configuration parameters
        self.options.declare('Kfsp', default=6.55, desc='Fuel density (lbs/gal)')
        self.options.declare('num_tanks', default=2, desc='Number of fuel tanks')
        self.options.declare('num_engines', default=1, desc='Number of engines')

    def setup(self):
        #nn = self.options['num_nodes']
        self.add_input('W_fuel',units='lb',desc='Full fuel weight')

        #self.add_output('heat_out', units='W', desc='Waste heat out',shape=(nn,))
        self.add_output('W_fuelsystem', units='lb', desc='Fuel system weight')
        self.declare_partials('W_fuelsystem','W_fuel')
            
    def compute(self, inputs, outputs):
        n_t = self.options['num_tanks']
        n_e = self.options['num_engines']
        Kfsp = self.options['Kfsp']
        #Torenbeek method, Roskam PVC6pg92eq6.24
        print('Fuel system weights')
        W_fs_Cessna = 0.4 * inputs['W_fuel'] / Kfsp
        print(W_fs_Cessna)
        # W_fs_Torenbeek = 80*(n_e+n_t-1) + 15*n_t**0.5 * (inputs['W_fuel']/Kfsp)**0.333
        # print(W_fs_Torenbeek)
        # W_fs_USAF = 2.49* ((inputs['W_fuel']/Kfsp)**0.6 * n_t**0.20 * n_e**0.13)**1.21
        # print(W_fs_USAF)
        W_fs_Raymer = 2.49 * (inputs['W_fuel']*1.0/Kfsp)**0.726*(0.5)**0.363
        print(W_fs_Raymer)
        outputs['W_fuelsystem'] = W_fs_Raymer

class EquipmentWeight_SmallTurboprop(ExplicitComponent):
    def setup(self):
        self.add_input('MTOW',units='lb',desc='Max takeoff weight')
        self.add_input('n_pax',desc='Number of passengers')
        self.add_input('l_f', units='ft', desc='fuselage width')
        self.add_input('AR_wing', desc='Wing aspect ratio')
        self.add_input('S_ref', units='ft**2', desc='Wing reference area')
        self.add_input('W_fuelsystem',units='lb', desc='Fuel system weight')
        self.add_output('W_equipment',units='lb',desc='Equipment weight')
    def compute(self, inputs, outputs):
        b = math.sqrt(inputs['S_ref']*inputs['AR_wing'])

        #Flight control system (unpowered)
        #Roskam PVC7p98eq7.2
        #Wfc_USAF = 1.066*inputs['MTOW']**0.626
        Wfc_Torenbeek = 0.23*inputs['MTOW']**0.666
        #Hydraulic system weight included in flight controls and LG weight
        Whydraulics = 0.2673*1*(inputs['l_f']*b)**0.937

        #Guesstimate of avionics weight
        Wavionics = 2.117*(np.array([110]))**0.933
        #Electrical system weight (NOT including elec propulsion)
        Welec = 12.57*(inputs['W_fuelsystem']+Wavionics)**0.51

        #pressurization and air conditioning from Roskam
        Wapi = 0.265*inputs['MTOW']**0.52 * inputs['n_pax']**0.68 * Wavionics**0.17 * 0.95
        Woxygen = 30 + 1.2*inputs['n_pax']
        #furnishings (Cessna method)
        Wfur = 0.412*inputs['n_pax']**1.145 * inputs['MTOW'] ** 0.489
        Wpaint = 0.003 * inputs['MTOW']

        outputs['W_equipment'] = Wfc_Torenbeek + Welec + Wavionics + Wapi + Woxygen + Wfur + Wpaint + Whydraulics



if __name__ == "__main__":
    from openmdao.api import IndepVarComp, Problem
    prob = Problem()
    prob.model = Group()
    dvs = prob.model.add_subsystem('dvs',IndepVarComp(),promotes_outputs=["*"])
    AR = 53.5**2/277.8
    dvs.add_output('MTOW',10450.,units='lb')
    dvs.add_output('S_ref',277.8,units='ft**2')
    dvs.add_output('AR_wing',AR)
    dvs.add_output('c4sweep_wing',0.0,units='deg')
    dvs.add_output('taper_wing',0.53)
    dvs.add_output('toverc_wing',0.18)
    dvs.add_output('V_H',235,units='kn')
    
    dvs.add_output('S_h',65.2,units='ft**2')
    dvs.add_output('AR_h',4.6)
    dvs.add_output('S_v',36,units='ft**2')
    dvs.add_output('AR_v',1.0)
    dvs.add_output('troot_h',0.8,units='ft')
    dvs.add_output('troot_v',0.8,units='ft')
    dvs.add_output('l_h',25,units='ft')

    dvs.add_output('l_f',35,units='ft')
    dvs.add_output('h_f',6.0,units='ft')
    dvs.add_output('w_f',5.5,units='ft')
    dvs.add_output('S_f',550,units='ft**2')
    dvs.add_output('V_C',180,units='kn') #IAS (converted from 315kt true at 28,000 )
    dvs.add_output('V_MO',240,units='kn')
    dvs.add_output('P_TO',1200,units='hp')
    dvs.add_output('W_fuel',2704,units='lb')
    dvs.add_output('n_pax', 9)
    dvs.add_output('q_cruise', 110, units='lb*ft**-2')
    dvs.add_output('MLW', 9920, units='lb')
    dvs.add_output('l_ng', 3, units='ft')
    dvs.add_output('l_mg', 3.3, units='ft')



    prob.model.add_subsystem('wing',WingWeight_SmallTurboprop(),promotes_inputs=["*"])

    prob.model.add_subsystem('empennage',EmpennageWeight_SmallTurboprop(),promotes_inputs=["*"])
    prob.model.add_subsystem('fuselage',FuselageWeight_SmallTurboprop(),promotes_inputs=["*"])
    prob.model.add_subsystem('nacelle',NacelleWeight_SmallTurboprop(),promotes_inputs=["*"])
    prob.model.add_subsystem('gear',LandingGearWeight_SmallTurboprop(),promotes_inputs=["*"])
    prob.model.add_subsystem('fuelsystem', FuelSystemWeight_SmallTurboprop(), promotes_inputs=["*"])
    prob.model.add_subsystem('equipment',EquipmentWeight_SmallTurboprop(), promotes_inputs=["*"])


    prob.setup()
    prob.run_model()
    OWE = (prob['wing.W_wing'] + prob['fuselage.W_fuselage'] + prob['nacelle.W_nacelle'] + prob['empennage.W_empennage'] + prob['gear.W_gear'])*1.5 + prob['fuelsystem.W_fuelsystem'] + prob['equipment.W_equipment'] +np.array([575+250])
    print('Wing weight:')
    print(prob['wing.W_wing'])
    print('Fuselage weight:')
    print(prob['fuselage.W_fuselage'])
    print('Empennage weight:')
    print(prob['empennage.W_empennage'])
    print('Nacelle weight:')
    print(prob['nacelle.W_nacelle'])
    print('Fuel system weight')
    print(prob['fuelsystem.W_fuelsystem'])
    print('Gear weight')
    print(prob['gear.W_gear'])
    print('Equipment weight')
    print(prob['equipment.W_equipment'])
    print(OWE)
    #data = prob.check_partials()

