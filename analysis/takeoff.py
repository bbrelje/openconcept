from openmdao.api import Problem, Group, IndepVarComp
from openmdao.api import ScipyOptimizeDriver, ExplicitComponent

import numpy as np
import scipy.sparse as sp

from openConcept.analysis.atmospherics.compute_atmos_props import ComputeAtmosphericProperties
from openConcept.utilities.math.simpson_integration import simpson_integral, simpson_partials, simpson_integral_every_node, simpson_partials_every_node, IntegrateQuantity
from openConcept.utilities.math import Sum, Adder, ElementMultiply, Combiner, Splitter
from openConcept.analysis.aerodynamics import PolarDrag, Lift, StallSpeed
from openConcept.utilities.dvlabel import DVLabel

class ComputeBalancedFieldLengthResidual(ExplicitComponent):
    def setup(self):
        self.add_input('takeoff_distance',units='m')
        self.add_input('takeoff_distance_abort',units='m')
        self.add_output('BFL_residual',units='m')
        self.declare_partials('BFL_residual','takeoff_distance',val=1)
        self.declare_partials('BFL_residual','takeoff_distance_abort',val=-1)
    def compute(self,inputs,outputs):
        outputs['BFL_residual'] = inputs['takeoff_distance'] - inputs['takeoff_distance_abort']


class TakeoffFlightConditions(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg. Number of time points is 2N+1")
        self.options.declare('vr_multiple',default=1.1, desc="Rotation speed multiple of Vstall")
        self.options.declare('v2_multiple',default=1.2, desc='Climb out multipile of Vstall')

    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        vrmult = self.options['vr_multiple']
        v2mult = self.options['v2_multiple']
        self.add_input('takeoff_h', val=0, units='m', desc='Takeoff runway altitude')
        self.add_input('takeoff_v1', val=75, units='m / s', desc='Takeoff decision airspeed')
        self.add_input('aero_Vstall_eas', val=90, units='m / s', desc='Flaps down stall airspeed')
        self.add_output('takeoff_vr', units='m/s', desc='Takeoff rotation speed')
        self.add_output('takeoff_v2',units='m/s', desc='Takeoff climbout speed')

        ## the total length of the vector is 3 * nn (3 integrated segments) + 2 scalar flight conditions
        # condition 1: v0 to v1 [nn points]
        # condition 2: v1 to vr (one engine out) [nn points]
        # condition 3: v1 to v0 (with braking) [nn points]
        # condition 4: average between vr and v2 [1 point]
        # condition 5: v2 [1 point]
        self.add_output('fltcond_Ueas_takeoff', units='m / s', desc='indicated airspeed at each timepoint',shape=(3*nn+2,))
        self.add_output('fltcond_h_takeoff', units='m', desc='altitude at each timepoint',shape=(3*nn+2,))
        linear_nn = np.linspace(0,1,nn)
        linear_rev_nn = np.linspace(1,0,nn)

        #the climb speeds only have influence over their respective mission segments
        self.declare_partials(['fltcond_Ueas_takeoff'],['takeoff_v1'],val=np.concatenate([linear_nn,linear_rev_nn,linear_rev_nn,np.zeros(2)]))
        self.declare_partials(['fltcond_Ueas_takeoff'],['aero_Vstall_eas'],val=np.concatenate([np.zeros(nn),linear_nn*vrmult,np.zeros(nn),np.array([(vrmult+v2mult)/2,v2mult])]))
        self.declare_partials(['fltcond_h_takeoff'],['takeoff_h'],val=np.ones(3*nn+2))
        self.declare_partials(['takeoff_vr'],['aero_Vstall_eas'],val=vrmult)
        self.declare_partials(['takeoff_v2'],['aero_Vstall_eas'],val=v2mult)


    def compute(self,inputs,outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = n_int_per_seg*2 + 1
        vrmult = self.options['vr_multiple']
        v2mult = self.options['v2_multiple']
        v1 = inputs['takeoff_v1'][0]
        vstall = inputs['aero_Vstall_eas'][0]
        vr = vrmult*vstall
        v2 = v2mult*vstall
        outputs['takeoff_vr'] = vr
        outputs['takeoff_v2'] = v2
        outputs['fltcond_h_takeoff'] = inputs['takeoff_h']*np.ones(3*nn+2)
        outputs['fltcond_Ueas_takeoff'] = np.concatenate([np.linspace(1,v1,nn),np.linspace(v1,vr,nn),np.linspace(v1,1,nn),np.array([(vr+v2)/2, v2])])


class TakeoffCLs(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg. Number of time points is 2N+1")
        self.options.declare('ground_CL',default=0.1,desc="Assumed CL during the takeoff roll")
    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        nn_tot = 3*nn+2
        self.add_input('weight',units='kg')
        self.add_input('fltcond_q_takeoff',units='N * m**-2',shape=(nn_tot,))
        self.add_input('geom_S_ref', units='m**2')
        self.add_output('aero_CL_takeoff',shape=(nn_tot,))
        #the partials only apply for the last two entries
        self.declare_partials(['aero_CL_takeoff'],['weight','geom_S_ref'],rows=np.arange(nn_tot-2,nn_tot),cols=np.zeros(2))
        self.declare_partials(['aero_CL_takeoff'],['fltcond_q_takeoff'],rows=np.arange(nn_tot-2,nn_tot),cols=np.arange(nn_tot-2,nn_tot))

    def compute(self,inputs,outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        nn_tot = 3*nn+2
        g = 9.80665 #m/s^2
        CLs = np.ones(nn_tot) * self.options['ground_CL']
        #the 1.2 load factor is an assumption from Raymer. May want to revisit or make an option /default in the future
        loadfactors = np.array([1.2,1.2])
        CLs[-2:] = loadfactors*inputs['weight'][-2:] * g / inputs['fltcond_q_takeoff'][-2:] / inputs['geom_S_ref'][-2:]
        outputs['aero_CL_takeoff'] = CLs
    def compute_partials(self,inputs,J):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        nn_tot = 3*nn+2
        g = 9.80665 #m/s^2
        #the 1.2 load factor is an assumption from Raymer. May want to revisit or make an option /default in the future
        loadfactors = np.array([1.2,1.2])        
        J['aero_CL_takeoff','weight'] = loadfactors * g / inputs['fltcond_q_takeoff'][-2:] / inputs['geom_S_ref'][-2:]   
        J['aero_CL_takeoff','fltcond_q_takeoff'] = - loadfactors*inputs['weight'][-2:] * g / inputs['fltcond_q_takeoff'][-2:]**2 / inputs['geom_S_ref'][-2:]   
        J['aero_CL_takeoff','geom_S_ref'] = - loadfactors*inputs['weight'][-2:] * g / inputs['fltcond_q_takeoff'][-2:] / inputs['geom_S_ref'][-2:]**2

class TakeoffAccels(ExplicitComponent):
    """This returns the INVERSE of the accelerations during the takeoff run
        This is due to the integration wrt velocity: int( dr/dt * dt / dv) dv = int( v / a) dv
    """
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg. Number of time points is 2N+1")
        self.options.declare('free_rolling_friction_coeff',default=0.03,desc='Rolling friction coefficient (no brakes)')
        self.options.declare('braking_friction_coeff',default=0.4,desc='Coefficient of friction whilst applying max braking')
    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        nn_tot = 3*nn+2
        self.add_input('weight',units='kg')
        self.add_input('aero_drag',units='N',shape=(nn_tot,))
        self.add_input('aero_lift',units='N',shape=(nn_tot,))
        self.add_input('thrust_takeoff',units='N',shape=(nn_tot,))
        self.add_output('_inverse_accel',units='s**2 / m',shape=(nn_tot,))
        arange=np.arange(0,nn_tot-2)
        self.declare_partials(['_inverse_accel'],['aero_drag','aero_lift','thrust_takeoff'],rows=arange,cols=arange)
        self.declare_partials(['_inverse_accel'],['weight'],rows=arange,cols=np.zeros(nn_tot-2))

    def compute(self,inputs,outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        nn_tot = 3*nn+2
        mu_free = self.options['free_rolling_friction_coeff']
        mu_brake = self.options['braking_friction_coeff']
        mu = np.concatenate([np.ones(2*nn)*mu_free,np.ones(nn)*mu_brake,np.zeros(2)])     
        g = 9.80665 #m/s^2
        # print('Thrust:' +str(inputs['thrust_takeoff']))
        # print('Drag:'+str(inputs['aero_drag']))
        # print('Rolling resistance:'+str(mu*(inputs['weight']*g-inputs['aero_lift'])))

        forcebal = inputs['thrust_takeoff'] - inputs['aero_drag'] - mu*(inputs['weight']*g-inputs['aero_lift'])
        # print('Force balance:'+str(forcebal))
        inv_accel = inputs['weight'] / forcebal 
        inv_accel[-2:] = np.zeros(2)
        outputs['_inverse_accel'] = inv_accel

    def compute_partials(self,inputs,J):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        nn_tot = 3*nn+2
        mu_free = self.options['free_rolling_friction_coeff']
        mu_brake = self.options['braking_friction_coeff']
        mu = np.concatenate([np.ones(2*nn)*mu_free,np.ones(nn)*mu_brake,np.zeros(2)])     
        g = 9.80665 #m/s^2
        forcebal = inputs['thrust_takeoff'] - inputs['aero_drag'] - mu*(inputs['weight']*g-inputs['aero_lift'])
        #(f/g)' = (f'g-fg')/g^2 where f is weight and g is forcebal
        ddweight = (1*forcebal - inputs['weight']*-mu*g)/forcebal**2
        ddweight=ddweight[:-2]
        dddrag = -inputs['weight'] / forcebal**2 * (-1)
        dddrag = dddrag[:-2]
        ddlift = -inputs['weight'] / forcebal**2 *mu
        ddlift = ddlift[:-2]
        ddthrust = - inputs['weight'] / forcebal**2
        ddthrust = ddthrust[:-2]

        J['_inverse_accel','thrust_takeoff'] = ddthrust
        J['_inverse_accel','aero_drag'] = dddrag
        J['_inverse_accel','aero_lift'] = ddlift
        J['_inverse_accel','weight'] = ddweight

class TakeoffV2ClimbAngle(ExplicitComponent):
    def setup(self):
        self.add_input('aero_drag_v2',units='N')
        self.add_input('weight',units='kg')
        self.add_input('thrust_takeoff_v2',units='N')
        self.add_output('takeoff_climb_gamma',units='rad')
    
        self.declare_partials(['takeoff_climb_gamma'],['weight','thrust_takeoff_v2','aero_drag_v2'])
    
    def compute(self,inputs,outputs):
        g = 9.80665 #m/s^2
        outputs['takeoff_climb_gamma'] = np.arcsin((inputs['thrust_takeoff_v2']-inputs['aero_drag_v2'])/inputs['weight']/g)
    
    def compute_partials(self,inputs,J):
        g = 9.80665 #m/s^2
        interior_qty = (inputs['thrust_takeoff_v2']-inputs['aero_drag_v2'])/inputs['weight']/g
        d_arcsin = 1/np.sqrt(1-interior_qty**2)
        J['takeoff_climb_gamma','thrust_takeoff_v2'] = d_arcsin/inputs['weight']/g
        J['takeoff_climb_gamma','aero_drag_v2'] = -d_arcsin/inputs['weight']/g
        J['takeoff_climb_gamma','weight'] = -d_arcsin*(inputs['thrust_takeoff_v2']-inputs['aero_drag_v2'])/inputs['weight']**2/g

class TakeoffTransition(ExplicitComponent):
    def initialize(self):
        self.options.declare('h_obstacle',default=10.66,desc='Obstacle clearance height in m')

    def setup(self):
        self.add_input('fltcond_Utrue_takeoff_vtrans',units='m/s')
        self.add_input('takeoff_climb_gamma',units='rad')
        self.add_output('s_transition',units='m')
        self.add_output('h_transition',units='m')
        self.declare_partials(['s_transition','h_transition'],['fltcond_Utrue_takeoff_vtrans','takeoff_climb_gamma'])

    def compute(self,inputs,outputs):
        hobs = self.options['h_obstacle']
        g = 9.80665 #m/s^2
        gam = inputs['takeoff_climb_gamma']
        ut = inputs['fltcond_Utrue_takeoff_vtrans']

        R = ut**2/0.2/g
        st = R*np.sin(gam)
        ht = R*(1-np.cos(gam))
        #alternate formula if the obstacle is cleared during transition
        if ht > hobs:
            st = np.sqrt(R**2-(R-hobs)**2)
            ht = hobs
        outputs['s_transition'] = st
        outputs['h_transition'] = ht

    def compute_partials(self,inputs,J):
        hobs = self.options['h_obstacle']
        g = 9.80665 #m/s^2
        gam = inputs['takeoff_climb_gamma']
        ut = inputs['fltcond_Utrue_takeoff_vtrans']
        R = ut**2/0.2/g
        dRdut =  2*ut/0.2/g
        st = R*np.sin(gam)
        ht = R*(1-np.cos(gam))
        #alternate formula if the obstacle is cleared during transition
        if ht > hobs:
            st = np.sqrt(R**2-(R-hobs)**2)
            dstdut = 1/2/np.sqrt(R**2-(R-hobs)**2) * (2*R*dRdut - 2*(R-hobs)*dRdut) 
            dstdgam = 0
            dhtdut = 0
            dhtdgam = 0
        else:
            dhtdut = dRdut*(1-np.cos(gam))
            dhtdgam = R*np.sin(gam)
            dstdut = dRdut*np.sin(gam)
            dstdgam = R*np.cos(gam)
        J['s_transition','takeoff_climb_gamma'] = dstdgam
        J['s_transition','fltcond_Utrue_takeoff_vtrans'] = dstdut
        J['h_transition','takeoff_climb_gamma'] = dhtdgam
        J['h_transition','fltcond_Utrue_takeoff_vtrans'] = dhtdut

        
class TakeoffClimb(ExplicitComponent):
    def initialize(self):
        self.options.declare('h_obstacle',default=10.66,desc='Obstacle clearance height in m')
    def setup(self):
        self.add_input('h_transition',units='m')
        self.add_input('takeoff_climb_gamma',units='rad')
        self.add_output('s_climb',units='m')
        self.declare_partials(['s_climb'],['h_transition','takeoff_climb_gamma'])

    def compute(self,inputs,outputs):
        hobs = self.options['h_obstacle']
        gam = inputs['takeoff_climb_gamma']
        ht = inputs['h_transition']
        sc = (hobs-ht)/np.tan(gam)
        outputs['s_climb'] = sc

    def compute_partials(self,inputs,J):
        hobs = self.options['h_obstacle']
        gam = inputs['takeoff_climb_gamma']
        ht = inputs['h_transition']
        sc = (hobs-ht)/np.tan(gam)
        J['s_climb','takeoff_climb_gamma'] = -(hobs-ht)/np.tan(gam)**2 * (1/np.cos(gam))**2
        J['s_climb','h_transition'] = -1/np.tan(gam)

class TakeoffTotalDistance(Group):
    """This high level component calculates lift, drag, rolling resistance during a takeoff roll, including one-engine-out and abort.
    Promote the following = ['geom_*','fltcond_*_takeoff','thrust_takeoff','fuel_flow_takeoff']
    Need geom_AR_wing, geom_Sref, fltcond_Utrue_takeoff, and some other quantities
    """
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg. Number of time points is 2N+1")
    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        nn_tot = 3*nn+2
        #===Some of the generic components take "weight" as an input. Need to re-label Takeoff Weight (TOW) as just weight
        dvlist = [['TOW','weight',2000.0,'kg']]
        self.add_subsystem('dvs',DVLabel(dvlist),promotes_inputs=["*"],promotes_outputs=["*"])

        #===We assume the takeoff starts at 1 m/s to avoid singularities at v=0
        const = self.add_subsystem('const',IndepVarComp())
        const.add_output('v0',val=1,units='m/s')
        const.add_output('reaction_time',val=3,units='s')
        
        #===Lift and Drag Calculations feed the EOMs for the takeoff roll===
        self.add_subsystem('CL',TakeoffCLs(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["geom_*","fltcond_*","weight"],)
        self.add_subsystem('drag',PolarDrag(num_nodes=nn_tot),promotes_inputs=['geom_*'],promotes_outputs=['aero_drag'])
        self.add_subsystem('lift',Lift(num_nodes=nn_tot),promotes_inputs=['geom_*'],promotes_outputs=['aero_lift'])
        self.connect('CL.aero_CL_takeoff','drag.fltcond_CL')
        self.connect('CL.aero_CL_takeoff','lift.fltcond_CL')

        #==The takeoff distance integrator numerically integrates the quantity (speed / acceleration) with respect to speed to obtain distance. Obtain this quantity:
        self.add_subsystem('accel',TakeoffAccels(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=['aero_lift','aero_drag','thrust_takeoff','weight'],promotes_outputs=['_inverse_accel'])
        self.add_subsystem('mult',ElementMultiply(num_nodes=nn_tot,input_names_lists=['_inverse_accel','fltcond_Utrue_takeoff'],output_names='_rate_to_integrate',units=['s**2/m','m/s']),promotes_inputs=['*'],promotes_outputs=['_*'])

        #==The following boilerplate splits the input flight conditions, thrusts, and fuel flows into the takeoff segments for further analysis
        inputs_to_split = ['_rate_to_integrate','fltcond_Utrue_takeoff','fuel_flow_takeoff','thrust_takeoff','aero_drag','aero_lift']
        segments_to_split_into = ['v0v1','v1vr','v1v0','vtrans','v2']
        units = ['s','m/s','kg/s','N','N','N']
        nn_each_segment = [nn,nn,nn,1,1]
        output_names_lists = []
        for input_name in inputs_to_split:
            output_names_list = []
            for segment in segments_to_split_into:
                output_names_list.append(input_name+'_'+segment)
            output_names_lists.append(output_names_list)
        splitter = self.add_subsystem('splitter',Splitter(num_nodes=nn_each_segment,units=units,input_names=inputs_to_split,output_names_lists=output_names_lists),promotes_inputs=["*"],promotes_outputs=["*"])
        
        #==Now integrate the three continuous segments: 0 to v1, v1 to rotation with reduced power if applicable, and hard braking
        self.add_subsystem('v0v1_dist',IntegrateQuantity(num_intervals=n_int_per_seg,quantity_units='m',diff_units='m/s',force_signs=True))
        self.add_subsystem('v1vr_dist',IntegrateQuantity(num_intervals=n_int_per_seg,quantity_units='m',diff_units='m/s',force_signs=True))
        self.add_subsystem('v1v0_dist',IntegrateQuantity(num_intervals=n_int_per_seg,quantity_units='m',diff_units='m/s',force_signs=True))

        self.connect('_rate_to_integrate_v0v1','v0v1_dist.rate')
        self.connect('_rate_to_integrate_v1vr','v1vr_dist.rate')
        self.connect('_rate_to_integrate_v1v0','v1v0_dist.rate')

        self.connect('const.v0','v0v1_dist.lower_limit')
        self.connect('const.v0','v1v0_dist.upper_limit')

        #==Next compute the transition and climb phase to the specified clearance height. First, need the steady climb-out angle at v2 speed
        self.add_subsystem('gamma',TakeoffV2ClimbAngle(),promotes_inputs=["aero_drag_v2","thrust_takeoff_v2","weight"],promotes_outputs=["takeoff_climb_gamma"])
        self.add_subsystem('transition',TakeoffTransition(),promotes_inputs=["fltcond_Utrue_takeoff_vtrans","takeoff_climb_gamma"],promotes_outputs=["h_transition","s_transition"])
        self.add_subsystem('climb',TakeoffClimb(),promotes_inputs=["takeoff_climb_gamma","h_transition"],promotes_outputs=["s_climb"])
        self.add_subsystem('reaction',ElementMultiply(num_nodes=1,input_names_lists=['takeoff_v1','reaction_time'],output_names='s_reaction',units=['m/s','s']))
        self.connect('const.reaction_time','reaction.reaction_time')
        self.connect('reaction.s_reaction','total_to_distance_continue.s_reaction')
        self.connect('reaction.s_reaction','total_to_distance_abort.s_reaction')
        self.add_subsystem('total_to_distance_continue',Adder(input_names_lists=['s_v0v1','s_v1vr','s_reaction','s_transition','s_climb'],output_names='takeoff_distance',units='m'),promotes_outputs=["*"])
        self.add_subsystem('total_to_distance_abort',Adder(input_names_lists=['s_v0v1','s_v1v0','s_reaction'],output_names='takeoff_distance_abort',units='m'),promotes_outputs=["*"])

        self.connect('v0v1_dist.delta_quantity',['total_to_distance_continue.s_v0v1','total_to_distance_abort.s_v0v1'])
        self.connect('v1vr_dist.delta_quantity','total_to_distance_continue.s_v1vr')
        self.connect('s_transition','total_to_distance_continue.s_transition')
        self.connect('s_climb','total_to_distance_continue.s_climb')
        self.connect('v1v0_dist.delta_quantity','total_to_distance_abort.s_v1v0')



class TakeoffAnalysisTest(Group):
    """This analysis group calculates TOFL and mission fuel burn as well as many other quantities for an example airplane. Elements may be overridden or replaced as needed.
        Should be instantiated as the top-level model

    """

    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")
        #self.options.declare('propmodel',desc='Propulsion model to use. Pass in the class, not an instance')

    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn_tot = (2*n_int_per_seg+1)*3 +2 #v0v1,v1vr,v1v0, vtr, v2
        nn = (2*n_int_per_seg+1)

        #Create holders for control and flight condition parameters. Add these as design variables as necessary for optimization when you define the prob

        dv_comp = self.add_subsystem('dv_comp',IndepVarComp(),promotes_outputs=["aero_CLmax_flapsdown","geom_*","takeoff_*","dv_*"])
        #eventually replace the following with an analysis module
        dv_comp.add_output('aero_CLmax_flapsdown',val=1.7)
        dv_comp.add_output('aero_polar_e',val=0.78)
        dv_comp.add_output('aero_polar_CD0',val=0.03)

        #wing geometry variables
        dv_comp.add_output('geom_S_ref',val=193.5,units='ft**2')
        dv_comp.add_output('geom_AR_wing',val=8.95)

        #design weights
        dv_comp.add_output('MTOW',val=3354,units='kg')

        #takeoff parameters
        dv_comp.add_output('takeoff_h',val=0,units='ft')
        dv_comp.add_output('takeoff_v1',val=85,units='kn')

        #propulsion parameters (rename this prefix at some point)
        dv_comp.add_output('dv_eng1_rating',850,units='hp')
        dv_comp.add_output('dv_prop1_diameter',2.3,units='m')




        #== Compute the stall speed (necessary for takeoff analysis)
        vstall = self.add_subsystem('vstall', StallSpeed(), promotes_inputs=["geom_S_ref","aero_CLmax_flapsdown"], promotes_outputs=["aero_Vstall_eas"])
        
        #==Calculate flight conditions for the takeoff and mission segments here
        conditions = self.add_subsystem('conds', TakeoffFlightConditions(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["takeoff_*","aero_*"],promotes_outputs=["fltcond_*","takeoff_*"])
        combiner = self.add_subsystem('combiner', Combiner(num_nodes=nn_tot,input_names_lists=[['fltcond_h_takeoff'],['fltcond_Ueas_takeoff']],units=['m','m/s'],output_names=['fltcond_h','fltcond_Ueas']), promotes_inputs=["*"],promotes_outputs=["*"])
        
        #==Calculate atmospheric properties and true airspeeds for all mission segments
        atmos = self.add_subsystem('atmos',ComputeAtmosphericProperties(num_nodes=nn_tot),promotes_inputs=["fltcond_h","fltcond_Ueas"],promotes_outputs=["fltcond_rho","fltcond_Utrue","fltcond_q"])
        
        #==Calculate engine thrusts and fuel flows. You will need to override this module to vary number of engines, prop architecture, etc
        # Your propulsion model must promote up a single variable called "thrust" and a single variable called "fuel_flow". You may need to sum these at a lower level in the prop model group
        # You will probably need to add more control parameters if you use multiple engines. You may also need to add implicit solver states if, e.g. turbogenerator power setting depends on motor power setting
        controls = self.add_subsystem('controls',IndepVarComp())
        prop = self.add_subsystem('propmodel',TurbopropPropulsionSystem(num_nodes=nn_tot),promotes_inputs=["fltcond_*","dv_*"],promotes_outputs=["fuel_flow","thrust"])
        
        #==Define control settings for the propulsion system.
        # Recall that all flight points including takeoff roll are calculated all at once
        # The structure of the takeoff vector should be:
        #[ nn points (takeoff at full power, v0 to v1),
        #  nn points (takeoff at engine-out power (if applicable), v1 to vr),
        #  nn points (hard braking at zero power or even reverse, vr to v0),
        # !CAUTION! 1 point (transition at OEI power (if applicable), v_trans)
        # !CAUTION! 1 point (v2 climb at OEI power (if app), v2)
        # ]
        controls.add_output('prop1_rpm',val=np.ones(nn_tot)*2000,units='rpm')
        throttle_vec = np.concatenate([np.ones(nn),np.ones(nn)*1.0,np.zeros(nn),np.ones(2)*1.0])
        controls.add_output('motor1_throttle',val=throttle_vec)

        #connect control settings to the various states in the propulsion model
        self.connect('controls.prop1_rpm','propmodel.prop1.rpm')
        self.connect('controls.motor1_throttle','propmodel.throttle')

        #now we have flight conditions and propulsion outputs for all flight conditions. Split into our individual analysis phases
        #== Leave this alone==#
        inputs_to_split = ['fltcond_q','fltcond_Utrue','fuel_flow','thrust']
        segments_to_split_into = ['takeoff']
        units = ['N * m**-2','m/s','kg/s','N']
        nn_each_segment = [nn_tot]
        output_names_lists = []
        for input_name in inputs_to_split:
            output_names_list = []
            for segment in segments_to_split_into:
                output_names_list.append(input_name+'_'+segment)
            output_names_lists.append(output_names_list)
        splitter = self.add_subsystem('splitter',Splitter(num_nodes=nn_each_segment,units=units,input_names=inputs_to_split,output_names_lists=output_names_lists),promotes_inputs=["*"],promotes_outputs=["*"])
        
        #==This next module calculates balanced field length, if applicable. Your optimizer or solver MUST implicitly drive the abort distance and oei takeoff distances to the same value by varying v1
        
        takeoff = self.add_subsystem('takeoff',TakeoffTotalDistance(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=['geom_*','fltcond_*_takeoff','thrust_takeoff','fuel_flow_takeoff'],promotes_outputs=['*'])
        self.connect('dv_comp.aero_polar_CD0','takeoff.drag.aero_polar_CD0')
        self.connect('dv_comp.aero_polar_e','takeoff.drag.aero_polar_e')
        self.connect('fltcond_q_takeoff','takeoff.drag.fltcond_q')
        self.connect('fltcond_q_takeoff','takeoff.lift.fltcond_q')
        self.connect('dv_comp.MTOW','vstall.weight')
        self.connect('dv_comp.MTOW','takeoff.TOW')
        self.connect('takeoff_v1','takeoff.v0v1_dist.upper_limit')
        self.connect('takeoff_v1','takeoff.v1vr_dist.lower_limit')
        self.connect('takeoff_vr','takeoff.v1vr_dist.upper_limit')
        self.connect('takeoff_v1','takeoff.v1v0_dist.lower_limit')


if __name__ == "__main__":
    from openConcept.examples.simple_turboprop import TurbopropPropulsionSystem
    prob = Problem()    
    prob.model= TakeoffAnalysisTest(num_integration_intervals_per_seg=4)
    #prob.model=TestGroup()

    prob.setup()
    prob.run_model()
    # print(prob['fltcond_Ueas'])
    # print(prob['fltcond_h'])
    # print(prob['fltcond_rho'])
    # print(prob['fuel_flow'])
    print('Stall speed'+str(prob['aero_Vstall_eas']))
    print('Rotate speed'+str(prob['takeoff_vr']))

    print('V0V1 dist: '+str(prob['takeoff.v0v1_dist.delta_quantity']))
    print('V1VR dist: '+str(prob['takeoff.v1vr_dist.delta_quantity']))
    print('Braking dist:'+str(prob['takeoff.v1v0_dist.delta_quantity']))
    print('Climb angle(rad):'+str(prob['takeoff_climb_gamma']))
    print('h_trans:'+str(prob['h_transition']))
    print('s_trans:'+str(prob['s_transition']))
    print('s_climb:'+str(prob['s_climb']))
    print('TO (continue):'+str(prob['takeoff_distance']))
    print('TO (abort):'+str(prob['takeoff_distance_abort']))

    
    #prob.model.list_inputs()
    #prob.model.list_outputs()
    #prob.check_partials(compact_print=True)
    #prob.check_totals(compact_print=True)
