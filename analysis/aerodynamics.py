from openmdao.api import ExplicitComponent
import numpy as np

class PolarDrag(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_nodes',default=1,desc="Number of nodes to compute")

    def setup(self):
        nn = self.options['num_nodes']
        arange = np.arange(0,nn)
        self.add_input('fltcond_CL', shape=(nn,))
        self.add_input('fltcond_q',units='N * m**-2', shape=(nn,))
        self.add_input('geom_S_ref',units='m **2')
        self.add_input('aero_polar_CD0')
        self.add_input('aero_polar_e')
        self.add_input('geom_AR_wing')
        self.add_output('aero_drag',units='N',shape=(nn,))


        self.declare_partials(['aero_drag'],['fltcond_CL','fltcond_q'],rows=arange,cols=arange)
        self.declare_partials(['aero_drag'],['geom_S_ref','geom_AR_wing','aero_polar_CD0','aero_polar_e'],rows=arange,cols=np.zeros(nn))

    def compute(self,inputs,outputs):
        outputs['aero_drag'] = inputs['fltcond_q']*inputs['geom_S_ref']*(inputs['aero_polar_CD0'] + inputs['fltcond_CL']**2 / np.pi / inputs['aero_polar_e'] / inputs['geom_AR_wing'])

    def compute_partials(self,inputs,J): 
        J['aero_drag','fltcond_q'] = inputs['geom_S_ref']*(inputs['aero_polar_CD0'] + inputs['fltcond_CL']**2 / np.pi / inputs['aero_polar_e'] / inputs['geom_AR_wing'])
        J['aero_drag','fltcond_CL'] = inputs['fltcond_q']*inputs['geom_S_ref']*(2 * inputs['fltcond_CL'] / np.pi / inputs['aero_polar_e'] / inputs['geom_AR_wing'])
        J['aero_drag','aero_polar_CD0'] = inputs['fltcond_q']*inputs['geom_S_ref']
        J['aero_drag','aero_polar_e'] = - inputs['fltcond_q']*inputs['geom_S_ref']*inputs['fltcond_CL']**2 / np.pi / inputs['aero_polar_e']**2 / inputs['geom_AR_wing']
        J['aero_drag','geom_S_ref'] = inputs['fltcond_q']*(inputs['aero_polar_CD0'] + inputs['fltcond_CL']**2 / np.pi / inputs['aero_polar_e'] / inputs['geom_AR_wing'])
        J['aero_drag','geom_AR_wing'] = - inputs['fltcond_q']*inputs['geom_S_ref'] * inputs['fltcond_CL']**2 / np.pi / inputs['aero_polar_e'] / inputs['geom_AR_wing']**2

class Lift(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_nodes',default=1,desc="Number of nodes to compute")

    def setup(self):
        nn = self.options['num_nodes']
        arange = np.arange(0,nn)
        self.add_input('fltcond_CL', shape=(nn,))
        self.add_input('fltcond_q',units='N * m**-2', shape=(nn,))
        self.add_input('geom_S_ref',units='m **2')

        self.add_output('aero_lift',units='N',shape=(nn,))
        self.declare_partials(['aero_lift'],['fltcond_CL','fltcond_q'],rows=arange,cols=arange)
        self.declare_partials(['aero_lift'],['geom_S_ref'],rows=arange,cols=np.zeros(nn))

    def compute(self,inputs,outputs):
        outputs['aero_lift'] = inputs['fltcond_q']*inputs['geom_S_ref']*inputs['fltcond_CL']

    def compute_partials(self,inputs,J): 
        J['aero_lift','fltcond_q'] = inputs['geom_S_ref']*inputs['fltcond_CL']
        J['aero_lift','fltcond_CL'] = inputs['fltcond_q']*inputs['geom_S_ref']
        J['aero_lift','geom_S_ref'] = inputs['fltcond_q']*inputs['fltcond_CL']

class StallSpeed(ExplicitComponent):
    # def initialize(self):
    #     #self.options.declare('num_nodes',default=1,desc="Number of nodes")
    def setup(self):
        self.add_input('weight',units='kg')
        self.add_input('geom_S_ref',units='m**2')
        self.add_input('aero_CLmax_flapsdown')
        self.add_output('aero_Vstall_eas',units='m/s')
        self.declare_partials(['aero_Vstall_eas'],['weight','geom_S_ref','aero_CLmax_flapsdown'])

    def compute(self,inputs,outputs):
        g = 9.80665 #m/s^2
        rho = 1.225 #kg/m3
        outputs['aero_Vstall_eas'] = np.sqrt(2*inputs['weight']*g / rho / inputs['geom_S_ref'] / inputs['aero_CLmax_flapsdown'])
    
    def compute_partials(self,inputs,J):
        g = 9.80665 #m/s^2
        rho = 1.225 #kg/m3
        J['aero_Vstall_eas','weight'] = 1 / np.sqrt(2*inputs['weight']*g / rho / inputs['geom_S_ref'] / inputs['aero_CLmax_flapsdown']) * g / rho / inputs['geom_S_ref'] / inputs['aero_CLmax_flapsdown']
        J['aero_Vstall_eas','geom_S_ref'] = - 1 / np.sqrt(2*inputs['weight']*g / rho / inputs['geom_S_ref'] / inputs['aero_CLmax_flapsdown']) * inputs['weight']*g / rho  / inputs['geom_S_ref'] **2 / inputs['aero_CLmax_flapsdown']
        J['aero_Vstall_eas','aero_CLmax_flapsdown'] = - 1 / np.sqrt(2*inputs['weight']*g / rho / inputs['geom_S_ref'] / inputs['aero_CLmax_flapsdown']) * inputs['weight']*g / rho / inputs['geom_S_ref'] / inputs['aero_CLmax_flapsdown'] **2

