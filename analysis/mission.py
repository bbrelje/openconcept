from openmdao.api import Problem, Group, IndepVarComp, BalanceComp, DirectSolver, NewtonSolver, DenseJacobian, ScipyKrylov
import numpy as np
import scipy.sparse as sp
from openConcept.analysis.atmospherics.compute_atmos_props import ComputeAtmosphericProperties
from openConcept.utilities.math.simpson_integration import simpson_integral, simpson_partials, simpson_integral_every_node, simpson_partials_every_node
from openConcept.utilities.math import Sum
from openConcept.analysis.aerodynamics import PolarDrag
from openmdao.api import ScipyOptimizeDriver, ExplicitComponent, ImplicitComponent, BalanceComp, ArmijoGoldsteinLS, NonlinearBlockGS
from openConcept.utilities.dvlabel import DVLabel

class ComputeDesignMissionResiduals(ExplicitComponent):
    def setup(self):
        self.add_input('MTOW',val=2000,units='kg')
        self.add_input('mission_total_fuel',val=180,units='kg')
        self.add_input('OEW',val=1500,units='kg')
        self.add_input('mission_design_payload',val=200,units='kg')
        self.add_input('W_fuel_max',val=400,units='kg')
        self.add_output('mission_fuel_capacity_margin',units='kg')
        self.add_output('mission_MTOW_margin',units='kg')
        self.declare_partials('mission_fuel_capacity_margin','mission_total_fuel',val=-1)
        self.declare_partials('mission_fuel_capacity_margin','W_fuel_max' , val=1)
        self.declare_partials('mission_MTOW_margin','MTOW',val=1)
        self.declare_partials(['mission_MTOW_margin'],['mission_total_fuel','OEW','mission_design_payload'],val=-1)
    def compute(self, inputs, outputs):
        outputs['mission_fuel_capacity_margin'] = inputs['W_fuel_max']-inputs['mission_total_fuel']
        outputs['mission_MTOW_margin'] = inputs['MTOW'] - inputs['mission_total_fuel'] - inputs['OEW'] - inputs['mission_design_payload']


class MissionFlightConditions(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")

    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        self.add_input('mission_vspeed_climb', val=5, units='m / s', desc='Vertical speed in the climb segment')
        self.add_input('mission_vspeed_desc', val=-2.5, units='m / s', desc='Vertical speed in the descent segment (should be neg)')
        self.add_input('mission_eas_climb', val=90, units='m / s', desc='Indicated airspeed during climb')
        self.add_input('mission_eas_cruise', val=100, units='m / s', desc='Cruise airspeed (indicated)')
        self.add_input('mission_eas_desc', val=80, units='m / s', desc='Descent airspeed (indicated)')
        self.add_input('mission_h_ground', val=0, units='m',desc='Airport altitude')
        self.add_input('mission_h_cruise', val=8000, units='m', desc='Cruise altitude')

        self.add_output('fltcond_Ueas_mission', units='m / s', desc='indicated airspeed at each timepoint',shape=(3*nn,))
        self.add_output('fltcond_h_mission', units='m', desc='altitude at each timepoint',shape=(3*nn,))
        self.add_output('fltcond_vspeed_mission', units='m / s', desc='vectorial representation of vertical speed',shape=(3*nn,))
        self.add_output('mission_time_to_climb', units ='s', desc='Time from ground level to cruise')
        self.add_output('mission_time_to_descend', units='s', desc='Time to descend to ground from cruise')
        self.add_output('mission_dt_climb',units='s', desc='Timestep in climb phase')
        self.add_output('mission_dt_desc', units='s', desc='Timestep in descent phase')

        #the climb speeds only have influence over their respective mission segments
        self.declare_partials(['fltcond_Ueas_mission'],['mission_eas_climb'],rows=np.arange(0,nn),cols=np.ones(nn)*0,val=np.ones(nn))
        self.declare_partials(['fltcond_Ueas_mission'],['mission_eas_cruise'],rows=np.arange(nn,2*nn),cols=np.ones(nn)*0,val=np.ones(nn))
        self.declare_partials(['fltcond_Ueas_mission'],['mission_eas_desc'],rows=np.arange(2*nn,3*nn),cols=np.ones(nn)*0,val=np.ones(nn))
        hcruisepartials = np.concatenate([np.linspace(0.0,1.0,nn),np.ones(nn),np.linspace(1.0,0.0,nn)])
        hgroundpartials = np.concatenate([np.linspace(1.0,0.0,nn),np.linspace(0.0,1.0,nn)])
        #the influence of each parameter linearly varies from 0 to 1 and vice versa on climb and descent. The partials are different lengths on purpose - no influence of ground on the mid-mission points so no partial derivative
        self.declare_partials(['fltcond_h_mission'],['mission_h_cruise'],rows=range(3*nn),cols=np.zeros(3*nn),val=hcruisepartials)
        self.declare_partials(['fltcond_h_mission'],['mission_h_ground'],rows=np.concatenate([np.arange(0,nn),np.arange(2*nn,3*nn)]),cols=np.zeros(2*nn),val=hgroundpartials)
        self.declare_partials(['fltcond_vspeed_mission'],['mission_vspeed_climb'],rows=range(nn),cols=np.zeros(nn),val=np.ones(nn))
        self.declare_partials(['fltcond_vspeed_mission'],['mission_vspeed_desc'],rows=np.arange(2*nn,3*nn),cols=np.zeros(nn),val=np.ones(nn))
        self.declare_partials(['mission_time_to_climb'],['mission_h_ground','mission_h_cruise','mission_vspeed_climb'])
        self.declare_partials(['mission_time_to_descend'],['mission_h_ground','mission_h_cruise','mission_vspeed_desc'])
        self.declare_partials(['mission_dt_climb'],['mission_h_ground','mission_h_cruise','mission_vspeed_climb'])
        self.declare_partials(['mission_dt_desc'],['mission_h_ground','mission_h_cruise','mission_vspeed_desc'])



    def compute(self,inputs,outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = n_int_per_seg*2 + 1
        hvec_climb = np.linspace(inputs['mission_h_ground'],inputs['mission_h_cruise'],nn)
        hvec_desc = np.linspace(inputs['mission_h_cruise'],inputs['mission_h_ground'],nn)
        hvec_cruise = np.ones(nn)*inputs['mission_h_cruise']
        outputs['fltcond_h_mission'] = np.concatenate([hvec_climb,hvec_cruise,hvec_desc])
        debug = np.concatenate([np.ones(nn)*inputs['mission_eas_climb'],np.ones(nn)*inputs['mission_eas_cruise'],np.ones(nn)*inputs['mission_eas_desc']])
        outputs['fltcond_Ueas_mission'] = np.concatenate([np.ones(nn)*inputs['mission_eas_climb'],np.ones(nn)*inputs['mission_eas_cruise'],np.ones(nn)*inputs['mission_eas_desc']])
        outputs['fltcond_vspeed_mission'] = np.concatenate([np.ones(nn)*inputs['mission_vspeed_climb'],np.ones(nn)*0.0,np.ones(nn)*inputs['mission_vspeed_desc']])
        outputs['mission_time_to_climb'] = (inputs['mission_h_cruise']-inputs['mission_h_ground'])/inputs['mission_vspeed_climb']
        outputs['mission_time_to_descend'] = (inputs['mission_h_ground']-inputs['mission_h_cruise'])/inputs['mission_vspeed_desc']
        outputs['mission_dt_climb'] = (inputs['mission_h_cruise']-inputs['mission_h_ground'])/inputs['mission_vspeed_climb']/(nn-1)
        outputs['mission_dt_desc'] =  (inputs['mission_h_ground']-inputs['mission_h_cruise'])/inputs['mission_vspeed_desc']/(nn-1)

    def compute_partials(self, inputs, J):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = n_int_per_seg*2 + 1
        J['mission_time_to_climb','mission_h_cruise'] = 1/inputs['mission_vspeed_climb']
        J['mission_time_to_climb','mission_h_ground'] = -1/inputs['mission_vspeed_climb']
        J['mission_time_to_climb','mission_vspeed_climb'] = -(inputs['mission_h_cruise']-inputs['mission_h_ground'])/(inputs['mission_vspeed_climb']**2)
        J['mission_time_to_descend','mission_h_cruise'] = -1/inputs['mission_vspeed_desc']
        J['mission_time_to_descend','mission_h_ground'] = 1/inputs['mission_vspeed_desc']
        J['mission_time_to_descend','mission_vspeed_desc'] = -(inputs['mission_h_ground']-inputs['mission_h_cruise'])/(inputs['mission_vspeed_desc']**2)
        J['mission_dt_climb','mission_h_cruise'] = 1/inputs['mission_vspeed_climb']/(nn-1)
        J['mission_dt_climb','mission_h_ground'] = -1/inputs['mission_vspeed_climb']/(nn-1)
        J['mission_dt_climb','mission_vspeed_climb'] = -(inputs['mission_h_cruise']-inputs['mission_h_ground'])/(inputs['mission_vspeed_climb']**2)/(nn-1)
        J['mission_dt_desc','mission_h_cruise'] = -1/inputs['mission_vspeed_desc']/(nn-1)
        J['mission_dt_desc','mission_h_ground'] = 1/inputs['mission_vspeed_desc']/(nn-1)
        J['mission_dt_desc','mission_vspeed_desc'] = -(inputs['mission_h_ground']-inputs['mission_h_cruise'])/(inputs['mission_vspeed_desc']**2)/(nn-1)

class MissionGroundspeeds(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")

    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        self.add_input('fltcond_vspeed_mission', units='m/s',shape=(3*nn,))
        self.add_input('fltcond_Utrue_mission', units='m/s',shape=(3*nn,))
        self.add_output('mission_groundspeed', units='m/s',shape=(3*nn,))
        self.add_output('fltcond_cosgamma_mission', shape=(3*nn,), desc='Cosine of the flight path angle')
        self.add_output('fltcond_singamma_mission', shape=(3*nn,), desc='sin of the flight path angle' )
        self.declare_partials(['mission_groundspeed','fltcond_cosgamma_mission','fltcond_singamma_mission'],['fltcond_vspeed_mission','fltcond_Utrue_mission'],rows=range(3*nn),cols=range(3*nn))

    def compute(self, inputs, outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        #compute the groundspeed on climb and desc
        groundspeed =  np.sqrt(inputs['fltcond_Utrue_mission']**2-inputs['fltcond_vspeed_mission']**2)
        outputs['mission_groundspeed'] = groundspeed
        outputs['fltcond_singamma_mission'] = inputs['fltcond_vspeed_mission'] / inputs['fltcond_Utrue_mission']
        outputs['fltcond_cosgamma_mission'] = groundspeed / inputs['fltcond_Utrue_mission']

    def compute_partials(self, inputs, J):
        groundspeed =  np.sqrt(inputs['fltcond_Utrue_mission']**2-inputs['fltcond_vspeed_mission']**2)
        J['mission_groundspeed','fltcond_vspeed_mission'] = (1/2) / np.sqrt(inputs['fltcond_Utrue_mission']**2-inputs['fltcond_vspeed_mission']**2) * (-2) * inputs['fltcond_vspeed_mission']
        J['mission_groundspeed','fltcond_Utrue_mission'] = (1/2) / np.sqrt(inputs['fltcond_Utrue_mission']**2-inputs['fltcond_vspeed_mission']**2) * 2 * inputs['fltcond_Utrue_mission']
        J['fltcond_singamma_mission','fltcond_vspeed_mission'] = 1 / inputs['fltcond_Utrue_mission']
        J['fltcond_singamma_mission','fltcond_Utrue_mission'] = - inputs['fltcond_vspeed_mission'] / inputs['fltcond_Utrue_mission'] ** 2
        J['fltcond_cosgamma_mission','fltcond_vspeed_mission'] = J['mission_groundspeed','fltcond_vspeed_mission'] / inputs['fltcond_Utrue_mission']
        J['fltcond_cosgamma_mission','fltcond_Utrue_mission'] = (J['mission_groundspeed','fltcond_Utrue_mission'] * inputs['fltcond_Utrue_mission'] - groundspeed) / inputs['fltcond_Utrue_mission']**2

class MissionClimbDescentRanges(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")
        
    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        self.add_input('mission_groundspeed', units='m/s',shape=(3*nn,))
        self.add_input('mission_time_to_climb', units='s')
        self.add_input('mission_time_to_descend', units='s')
        self.add_output('mission_descent_range',units='m')
        self.add_output('mission_climb_range',units='m')
        self.declare_partials(['mission_climb_range'],['mission_groundspeed'],rows=np.ones(nn)*0,cols=range(nn))
        self.declare_partials(['mission_descent_range'],['mission_groundspeed'],rows=np.ones(nn)*0,cols=np.arange(2*nn,3*nn))
        self.declare_partials(['mission_climb_range'],['mission_time_to_climb'])
        self.declare_partials(['mission_descent_range'],['mission_time_to_descend'])

    def compute(self, inputs, outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)

        groundspeed = inputs['mission_groundspeed']
        #compute distance traveled during climb and desc using Simpson's rule
        dt_climb = inputs['mission_time_to_climb'] / (nn-1)
        dt_desc = inputs['mission_time_to_descend'] / (nn-1)
        simpsons_vec = np.ones(nn)
        simpsons_vec[1:nn-1:2] = 4
        simpsons_vec[2:nn-1:2] = 2
        outputs['mission_climb_range'] = np.sum(simpsons_vec*groundspeed[0:nn])*dt_climb/3
        outputs['mission_descent_range'] = np.sum(simpsons_vec*groundspeed[2*nn:3*nn])*dt_desc/3

    def compute_partials(self, inputs, J):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)

        groundspeed = inputs['mission_groundspeed']
        simpsons_vec = np.ones(nn)
        simpsons_vec[1:nn-1:2] = 4
        simpsons_vec[2:nn-1:2] = 2

        J['mission_climb_range','mission_time_to_climb'] = np.sum(simpsons_vec*groundspeed[0:nn])/3/(nn-1)
        J['mission_descent_range','mission_time_to_descend'] = np.sum(simpsons_vec*groundspeed[2*nn:3*nn])/3/(nn-1)
        J['mission_climb_range','mission_groundspeed'] = simpsons_vec * inputs['mission_time_to_climb'] / (nn-1) / 3
        J['mission_descent_range','mission_groundspeed'] = simpsons_vec * inputs['mission_time_to_descend'] / (nn-1) / 3

class MissionTimings(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
    
        self.add_input('mission_groundspeed',units='m/s',shape=(3*nn,))
        self.add_input('mission_climb_range', units='m')
        self.add_input('mission_descent_range', units='m')
        self.add_input('mission_range',units='m')
        self.add_output('mission_cruise_range',units='m')
        self.add_output('mission_cruise_time',units='s')
        self.add_output('mission_dt_cruise',units="s")
        self.declare_partials(['mission_cruise_range'],['mission_climb_range'],val=-1.0)
        self.declare_partials(['mission_cruise_range'],['mission_descent_range'],val=-1.0)
        self.declare_partials(['mission_cruise_range'],['mission_range'],val=1.0)
        self.declare_partials(['mission_cruise_time'],['mission_groundspeed'],rows=np.zeros(nn),cols=np.arange(nn,2*nn))
        self.declare_partials(['mission_cruise_time'],['mission_climb_range','mission_descent_range','mission_range'])
        self.declare_partials(['mission_dt_cruise'],['mission_groundspeed'],rows=np.zeros(nn),cols=np.arange(nn,2*nn))
        self.declare_partials(['mission_dt_cruise'],['mission_climb_range','mission_descent_range','mission_range'])

    def compute(self, inputs, outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
    
        simpsons_vec = np.ones(nn)
        simpsons_vec[1:nn-1:2] = 4
        simpsons_vec[2:nn-1:2] = 2

        #compute the cruise distance
        r_cruise = inputs['mission_range'] - inputs['mission_climb_range'] - inputs['mission_descent_range']
        if r_cruise < 0:
            raise ValueError('Cruise calculated to be less than 0. Change climb and descent rates and airspeeds or increase range')
        dt_cruise = 3*r_cruise/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])
        t_cruise = dt_cruise*(nn-1)

        outputs['mission_cruise_time'] = t_cruise
        outputs['mission_cruise_range'] = r_cruise
        outputs['mission_dt_cruise'] = dt_cruise

    def compute_partials(self, inputs, J):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)

        simpsons_vec = np.ones(nn)
        simpsons_vec[1:nn-1:2] = 4
        simpsons_vec[2:nn-1:2] = 2

        #compute the cruise distance
        r_cruise = inputs['mission_range'] - inputs['mission_climb_range'] - inputs['mission_descent_range']
        J['mission_cruise_time','mission_groundspeed'] = -3*r_cruise/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])**2 * (nn-1) * (simpsons_vec)
        J['mission_cruise_time','mission_climb_range'] = -3/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])*(nn-1)
        J['mission_cruise_time','mission_descent_range'] = -3/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])*(nn-1)
        J['mission_cruise_time','mission_range'] = 3/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])*(nn-1)

        J['mission_dt_cruise','mission_groundspeed'] = -3*r_cruise/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])**2  * (simpsons_vec)
        J['mission_dt_cruise','mission_climb_range'] = -3/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])
        J['mission_dt_cruise','mission_descent_range'] = -3/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])
        J['mission_dt_cruise','mission_range'] = 3/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])

class MissionSegmentFuelBurns(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")


    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
    
        self.add_input('fuel_flow_mission',units='kg/s',shape=(3*nn,))
        self.add_input('mission_dt_climb', units='s')
        self.add_input('mission_dt_desc', units='s')
        self.add_input('mission_dt_cruise', units='s')

        self.add_output('mission_segment_fuel',units='kg',shape=(3*(nn-1)))
        #use dummy inputs for dt and q, just want the shapes
        wrt_q, wrt_dt = simpson_partials_every_node(np.ones(3),np.ones(3*nn),n_segments=3,n_simpson_intervals_per_segment=n_int_per_seg)

        self.declare_partials(['mission_segment_fuel'],['fuel_flow_mission'],rows=wrt_q[0],cols=wrt_q[1])
        self.declare_partials(['mission_segment_fuel'],['mission_dt_climb'],rows=wrt_dt[0][0],cols=wrt_dt[1][0])
        self.declare_partials(['mission_segment_fuel'],['mission_dt_cruise'],rows=wrt_dt[0][1],cols=wrt_dt[1][1])
        self.declare_partials(['mission_segment_fuel'],['mission_dt_desc'],rows=wrt_dt[0][2],cols=wrt_dt[1][2])

    def compute(self,inputs,outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1) 
        ff = inputs['fuel_flow_mission']
        dts =  [inputs['mission_dt_climb'], inputs['mission_dt_cruise'],inputs['mission_dt_desc']]
        int_ff, delta_ff = simpson_integral_every_node(dts,ff,n_segments=3,n_simpson_intervals_per_segment=n_int_per_seg)

        outputs['mission_segment_fuel'] = delta_ff

    def compute_partials(self,inputs,J):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1) 
        ff = inputs['fuel_flow_mission']
        dts =  [inputs['mission_dt_climb'], inputs['mission_dt_cruise'],inputs['mission_dt_desc']]

        wrt_q, wrt_dt = simpson_partials_every_node(dts,ff,n_segments=3,n_simpson_intervals_per_segment=n_int_per_seg)

        J['mission_segment_fuel','fuel_flow_mission'] = wrt_q[2]
        J['mission_segment_fuel','mission_dt_climb'] =  wrt_dt[2][0]
        J['mission_segment_fuel','mission_dt_cruise'] = wrt_dt[2][1]
        J['mission_segment_fuel','mission_dt_desc'] = wrt_dt[2][2]

class MissionSegmentWeights(ExplicitComponent):
    
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")
    
    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        self.add_input('mission_segment_fuel',units='kg',shape=(3*(nn-1),))
        self.add_input('mission_weight_takeoff',units='kg')
        self.add_output('mission_weight',units='kg',shape=(3*nn,))

        n_seg = 3
        jacmat = np.tril(np.ones((n_seg*(nn-1),n_seg*(nn-1))))
        jacmat = np.insert(jacmat,0,np.zeros(n_seg*(nn-1)),axis=0)
        for i in range(1,n_seg):
            duplicate_row = jacmat[nn*i-1,:]
            jacmat = np.insert(jacmat,nn*i,duplicate_row,axis=0)

        self.declare_partials(['mission_weight'],['mission_segment_fuel'],val=sp.csr_matrix(jacmat))
        self.declare_partials(['mission_weight'],['mission_weight_takeoff'],rows=range(3*nn),cols=np.zeros(3*nn),val=np.ones(3*nn))

    def compute(self,inputs,outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1) 
        #first endpoint needs to be the takeoff weight; insert a zero to make them the same length
        n_seg = 3
        segweights = np.insert(inputs['mission_segment_fuel'],0,0)
        weights = np.cumsum(segweights)
        for i in range(1,n_seg):
            duplicate_row = weights[i*nn-1]
            weights = np.insert(weights,i*nn,duplicate_row)
        outputs['mission_weight'] = np.ones(3*nn)*inputs['mission_weight_takeoff'] + weights

class MissionSegmentCL(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")

    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        n_seg = 3
        arange = np.arange(0,n_seg*nn)
        self.add_input('mission_weight',units='kg', shape=(n_seg*nn,))
        self.add_input('fltcond_q_mission',units='N * m**-2', shape=(n_seg*nn,))
        self.add_input('geom_S_ref',units='m **2')
        self.add_input('fltcond_cosgamma_mission', shape=(n_seg*nn,))
        self.add_output('fltcond_CL_mission',shape=(n_seg*nn,))


        self.declare_partials(['fltcond_CL_mission'],['mission_weight','fltcond_q_mission',"fltcond_cosgamma_mission"],rows=arange,cols=arange)
        self.declare_partials(['fltcond_CL_mission'],['geom_S_ref'],rows=arange,cols=np.zeros(n_seg*nn))

    def compute(self,inputs,outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1) 
        #first endpoint needs to be the takeoff weight; insert a zero to make them the same length
        n_seg = 3

        g = 9.80665 #m/s^2
        outputs['fltcond_CL_mission'] = inputs['fltcond_cosgamma_mission']*g*inputs['mission_weight']/inputs['fltcond_q_mission']/inputs['geom_S_ref']

    def compute_partials(self,inputs,J):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1) 
        #first endpoint needs to be the takeoff weight; insert a zero to make them the same length
        n_seg = 3

        g = 9.80665 #m/s^2
        J['fltcond_CL_mission','mission_weight'] = inputs['fltcond_cosgamma_mission']*g/inputs['fltcond_q_mission']/inputs['geom_S_ref']
        J['fltcond_CL_mission','fltcond_q_mission'] = - inputs['fltcond_cosgamma_mission']*g*inputs['mission_weight'] / inputs['fltcond_q_mission']**2 / inputs['geom_S_ref']
        J['fltcond_CL_mission','geom_S_ref'] = - inputs['fltcond_cosgamma_mission']*g*inputs['mission_weight'] / inputs['fltcond_q_mission'] / inputs['geom_S_ref']**2
        J['fltcond_CL_mission','fltcond_cosgamma_mission'] = g*inputs['mission_weight']/inputs['fltcond_q_mission']/inputs['geom_S_ref']

# class PolarDrag(ExplicitComponent):
#     def initialize(self):
#         self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")

#     def setup(self):
#         n_int_per_seg = self.options['num_integration_intervals_per_seg']
#         nn = (n_int_per_seg*2 + 1)
#         n_seg = 3
#         arange = np.arange(0,n_seg*nn)
#         self.add_input('fltcond_CL_mission', shape=(n_seg*nn,))
#         self.add_input('fltcond_q_mission',units='N * m**-2', shape=(n_seg*nn,))
#         self.add_input('geom_S_ref',units='m **2')
#         self.add_input('aero_polar_CD0')
#         self.add_input('aero_polar_e')
#         self.add_input('geom_AR_wing')
#         self.add_output('aero_drag',units='N',shape=(n_seg*nn,))


#         self.declare_partials(['aero_drag'],['fltcond_CL_mission','fltcond_q_mission'],rows=arange,cols=arange)
#         self.declare_partials(['aero_drag'],['geom_S_ref','geom_AR_wing','aero_polar_CD0','aero_polar_e'],rows=arange,cols=np.zeros(n_seg*nn))

#     def compute(self,inputs,outputs):
#         n_int_per_seg = self.options['num_integration_intervals_per_seg']
#         nn = (n_int_per_seg*2 + 1) 
#         #first endpoint needs to be the takeoff weight; insert a zero to make them the same length
#         outputs['aero_drag'] = inputs['fltcond_q_mission']*inputs['geom_S_ref']*(inputs['aero_polar_CD0'] + inputs['fltcond_CL_mission']**2 / np.pi / inputs['aero_polar_e'] / inputs['geom_AR_wing'])

#     def compute_partials(self,inputs,J):
#         n_int_per_seg = self.options['num_integration_intervals_per_seg']
#         nn = (n_int_per_seg*2 + 1) 
#         #first endpoint needs to be the takeoff weight; insert a zero to make them the same length

#         J['aero_drag','fltcond_q_mission'] = inputs['geom_S_ref']*(inputs['aero_polar_CD0'] + inputs['fltcond_CL_mission']**2 / np.pi / inputs['aero_polar_e'] / inputs['geom_AR_wing'])
#         J['aero_drag','fltcond_CL_mission'] = inputs['fltcond_q_mission']*inputs['geom_S_ref']*(2 * inputs['fltcond_CL_mission'] / np.pi / inputs['aero_polar_e'] / inputs['geom_AR_wing'])
#         J['aero_drag','aero_polar_CD0'] = inputs['fltcond_q_mission']*inputs['geom_S_ref']
#         J['aero_drag','aero_polar_e'] = - inputs['fltcond_q_mission']*inputs['geom_S_ref']*inputs['fltcond_CL_mission']**2 / np.pi / inputs['aero_polar_e']**2 / inputs['geom_AR_wing']
#         J['aero_drag','geom_S_ref'] = inputs['fltcond_q_mission']*(inputs['aero_polar_CD0'] + inputs['fltcond_CL_mission']**2 / np.pi / inputs['aero_polar_e'] / inputs['geom_AR_wing'])
#         J['aero_drag','geom_AR_wing'] = - inputs['fltcond_q_mission']*inputs['geom_S_ref'] * inputs['fltcond_CL_mission']**2 / np.pi / inputs['aero_polar_e'] / inputs['geom_AR_wing']**2

class ThrustResidual(ImplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")
    
    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        n_seg = 3
        arange = np.arange(0,n_seg*nn)
        self.add_input('aero_drag', units='N',shape=(n_seg*nn,))
        self.add_input('fltcond_singamma_mission', shape=(n_seg*nn,))
        self.add_input('mission_weight',units='kg', shape=(n_seg*nn,))
        self.add_input('thrust_mission', units='N', shape=(n_seg*nn,))
        self.add_output('throttle', shape=(n_seg*nn,))
        self.declare_partials(['throttle'],['aero_drag'],val=-sp.eye(nn*n_seg))
        self.declare_partials(['throttle'],['thrust_mission'],val=sp.eye(nn*n_seg))
        self.declare_partials(['throttle'], ['fltcond_singamma_mission','mission_weight'],rows=arange,cols=arange)

    def apply_nonlinear(self, inputs, outputs, residuals):
        g = 9.80665 #m/s^2
        debug_nonlinear = True
        if debug_nonlinear:
            print('Thrust: ' + str(inputs['thrust_mission']))
            print('Drag: '+ str(inputs['aero_drag']))
            print('mgsingamma: ' + str(inputs['mission_weight']*g*inputs['fltcond_singamma_mission']))
            print('Throttle: ' + str(outputs['throttle']))

        residuals['throttle'] = inputs['thrust_mission'] - inputs['aero_drag'] - inputs['mission_weight']*g*inputs['fltcond_singamma_mission']


    def linearize(self, inputs, outputs, partials):
        g = 9.80665 #m/s^2
        partials['throttle','mission_weight'] = -g*inputs['fltcond_singamma_mission'] 
        partials['throttle','fltcond_singamma_mission'] = -g*inputs['mission_weight']

class ExplicitThrustResidual(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")
    
    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        n_seg = 3
        arange = np.arange(0,n_seg*nn)
        self.add_input('aero_drag', units='N',shape=(n_seg*nn,))
        self.add_input('fltcond_singamma_mission', shape=(n_seg*nn,))
        self.add_input('mission_weight',units='kg', shape=(n_seg*nn,))
        self.add_input('thrust_mission', units='N', shape=(n_seg*nn,))
        self.add_output('thrust_residual', shape=(n_seg*nn,))
        self.declare_partials(['thrust_residual'],['aero_drag'],rows=arange,cols=arange,val=-np.ones(nn*n_seg))
        self.declare_partials(['thrust_residual'],['thrust_mission'],rows=arange,cols=arange,val=np.ones(nn*n_seg))
        self.declare_partials(['thrust_residual'], ['fltcond_singamma_mission','mission_weight'],rows=arange,cols=arange)

    def compute(self, inputs, outputs):
        g = 9.80665 #m/s^2
        debug_nonlinear = False
        if debug_nonlinear:
            print('Thrust: ' + str(inputs['thrust_mission']))
            print('Drag: '+ str(inputs['aero_drag']))
            print('mgsingamma: ' + str(inputs['mission_weight']*g*inputs['fltcond_singamma_mission']))
            print('Throttle: ' + str(outputs['throttle']))

        outputs['thrust_residual'] = inputs['thrust_mission'] - inputs['aero_drag'] - inputs['mission_weight']*g*inputs['fltcond_singamma_mission']


    def compute_partials(self, inputs, J):
        g = 9.80665 #m/s^2
        J['thrust_residual','mission_weight'] = -g*inputs['fltcond_singamma_mission'] 
        J['thrust_residual','fltcond_singamma_mission'] = -g*inputs['mission_weight']

class MissionNoReserves(Group):
    """This analysis group calculates fuel burns, weights, and segment times

    """

    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")

    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn_tot = (2*n_int_per_seg+1)*3 #climb, cruise, descent
        #Create holders for control and flight condition parameters


        #OEW 2177kg
        #4pax 500kg
        #MTOW 3354kg
        #max fuel 921kg

        # dvs = self.add_subsystem('dvs',IndepVarComp(),promotes_outputs=["*"])
        # dvs.add_output('mission_vspeed_climb',1500,units='ft/min')
        # dvs.add_output('mission_vspeed_desc',-600,units='ft/min')
        # dvs.add_output('mission_h_cruise',28000,units='ft')
        # dvs.add_output('mission_h_ground',0,units='km')
        # dvs.add_output('mission_eas_climb',124,units='kn')
        # dvs.add_output('mission_eas_cruise',201,units='kn')
        # dvs.add_output('mission_eas_desc',140,units='kn')
        # dvs.add_output('mission_range',1250,units='NM')
        # dvs.add_output('mission_weight_takeoff',3374,units='kg')

        # dvs.add_output('dv_prop1_diameter',2.5,units='m')
        # # dvs.add_output('dv_motor1_rating',1.0,units='MW')
        # # dvs.add_output('dv_gen1_rating',1.05,units='MW')
        # dvs.add_output('dv_eng1_rating',850,units='hp')
        # # dvs.add_output('dv_batt1_weight',2000,units='kg')
        # dvs.add_output('geom_S_ref',18,units='m**2')

        # dvs.add_output('aero_polar_CD0',0.019)
        # dvs.add_output('aero_polar_e',0.78)
        # dvs.add_output('geom_AR_wing',8.93)
        dvlist = [['fltcond_q_mission','fltcond_q',100*np.ones(nn_tot),'Pa']]
        self.add_subsystem('dvs',DVLabel(dvlist),promotes_inputs=["*"],promotes_outputs=["*"])

        groundspeeds = self.add_subsystem('gs',MissionGroundspeeds(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["fltcond_*"],promotes_outputs=["mission_groundspeed","fltcond_*"])
        ranges = self.add_subsystem('ranges',MissionClimbDescentRanges(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["mission_groundspeed","mission_time*"],promotes_outputs=["mission_climb_range","mission_descent_range"])
        timings = self.add_subsystem('timings',MissionTimings(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["mission_*range","mission_groundspeed"],promotes_outputs=["mission_cruise_range","mission_cruise_time","mission_dt_cruise"])
        
        fbs = self.add_subsystem('fuelburn',MissionSegmentFuelBurns(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["fuel_flow_mission","mission_dt*"],promotes_outputs=["mission_segment_fuel"])
        wts = self.add_subsystem('weights',MissionSegmentWeights(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["mission_segment_fuel","mission_weight_takeoff"],promotes_outputs=["mission_weight"])
        CLs = self.add_subsystem('CLs',MissionSegmentCL(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["mission_weight","fltcond_q_mission",'fltcond_cosgamma_mission',"geom_S_ref"],promotes_outputs=["fltcond_CL_mission"])
        drag = self.add_subsystem('drag',PolarDrag(num_nodes=nn_tot),promotes_inputs=["fltcond_*","aero_*","geom_*"],promotes_outputs=["aero_drag"])
        self.connect('fltcond_CL_mission','fltcond_CL')
        #TODO connnect the drag polar from higher level
        #td = self.add_subsystem('thrust',ThrustResidual(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["fltcond_singamma_mission","aero_drag","mission_weight*","thrust_mission"])
        td = self.add_subsystem('thrust_resid',ExplicitThrustResidual(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["fltcond_singamma_mission","aero_drag","mission_weight*","thrust_mission"])
        totals = self.add_subsystem('totals',Sum(num_nodes=nn_tot-3, input_names='mission_segment_fuel', output_names='mission_total_fuel', units='kg',scaling_factors=-1),promotes_inputs=["*"], promotes_outputs=["*"])




if __name__ == "__main__":

    from openConcept.analysis.mission import MissionAnalysisTest
    from openConcept.examples.simple_turboprop import TurbopropPropulsionSystem

    import matplotlib.pyplot as plt
    prob = Problem()
    
    prob.model= MissionAnalysisTest(num_integration_intervals_per_seg=3,propmodel=TurbopropPropulsionSystem)
    prob.model.nonlinear_solver= NewtonSolver()
    prob.model.linear_solver = DirectSolver()
    prob.model.nonlinear_solver.options['solve_subsystems'] = True
    prob.model.nonlinear_solver.options['maxiter'] = 10
    prob.model.nonlinear_solver.options['atol'] = 1e-5
    prob.model.nonlinear_solver.options['rtol'] = 1e-5
    prob.model.nonlinear_solver.linesearch = ArmijoGoldsteinLS()
    prob.model.nonlinear_solver.linesearch.options['maxiter'] = 2
#    prob.model.nonlinear_solver.options['max_sub_solves'] = 1

    prob.driver = ScipyOptimizeDriver()
    prob.model.add_design_var('mission_h_cruise', lower=1000, upper=30000)
    prob.model.add_design_var('mission_vspeed_climb',lower=500,upper=3000)
    prob.model.add_design_var('mission_eas_climb',lower=85,upper=300)
    prob.model.add_design_var('mission_eas_cruise',lower=150,upper=300)
    prob.model.add_constraint('thrust.throttle',upper=np.ones(15)*0.95)
    prob.model.add_objective('mission_total_fuel')

    prob.setup()
    prob['thrust.throttle'] = np.ones(21)*0.7
    #prob['thrust.eng_throttle'] = np.ones(15)

    prob.run_model()
    #prob.run_driver()
    print(prob['mission_h_cruise'])

    # # print "------Prop 1-------"
    # print('Thrust: ' + str(prob['propmodel.prop1.thrust']))
    # plt.plot(prob['propmodel.prop1.thrust'])
    # plt.show()

    # print('Weight: ' + str(prob['propmodel.prop1.component_weight']))
    dtclimb = prob['mission_dt_climb']
    dtcruise = prob['mission_dt_cruise']
    dtdesc = prob['mission_dt_desc']
    n_int = 3
    timevec = np.concatenate([np.linspace(0,2*n_int*dtclimb,2*n_int+1),np.linspace(2*n_int*dtclimb,2*n_int*dtclimb+2*n_int*dtcruise,2*n_int+1),np.linspace(2*n_int*(dtclimb+dtcruise),2*n_int*(dtclimb+dtcruise+dtdesc),2*n_int+1)])
    plots = True
    if plots:
        print('Flight conditions')
        plt.figure(1)
        plt.plot(timevec, prob['conds.fltcond_Ueas_mission'],'b.')
        plt.plot(timevec, prob['atmos.trueairspeed.fltcond_Utrue_mission'],'b-')
        plt.plot(timevec, prob['gs.mission_groundspeed'],'g-')
        plt.title('Equivalent and true airspeed vs gs')

        print('Propulsion conditions')
        plt.figure(2)
        plt.plot(timevec, prob['thrust'])
        plt.title('Thrust')

        plt.figure(3)
        plt.plot(timevec, prob['fuel_flow_mission'])
        plt.title('Fuel flow')

        plt.figure(4)
        # plt.plot(np.delete(timevec,[0,20,41]),np.cumsum(prob['mission_segment_fuel']))
        plt.plot(timevec,prob['mission_weight'])
        plt.title('Weight')

        plt.figure(5)
        plt.plot(timevec,prob['fltcond_CL_mission'])
        plt.title('CL')

        plt.figure(6)
        plt.plot(timevec,prob['aero_drag'])
        plt.title('Drag')

        plt.figure(7)
        plt.plot(timevec,prob['propmodel.eng1.shaft_power_out'])
        plt.title('Shaft power')
        plt.show()
    print('Total fuel flow (totalizer):' + str(prob['mission_total_fuel']))
    print('Total fuel flow:' + str(np.sum(prob['mission_segment_fuel'])))
    

    #prob.model.list_inputs()
    #prob.model.list_outputs()
    #prob.check_partials(compact_print=True)
    #prob.check_totals(compact_print=True)
