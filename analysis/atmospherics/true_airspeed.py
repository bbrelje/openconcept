from __future__ import division
import numpy as np

from openmdao.api import ExplicitComponent

class TrueAirspeedComp(ExplicitComponent):

    def initialize(self):
        self.options.declare('num_nodes', types=int)

    def setup(self):
        num_points = self.options['num_nodes']

        self.add_input('fltcond_Ueas', units='m / s', shape=num_points)
        self.add_input('fltcond_rho', units='kg * m**-3', shape=num_points)
        self.add_output('fltcond_Utrue', units='m / s', shape=num_points)

        arange = np.arange(num_points)
        self.declare_partials('fltcond_Utrue', 'fltcond_Ueas', rows=arange, cols=arange)
        self.declare_partials('fltcond_Utrue', 'fltcond_rho', rows=arange, cols=arange)

    def compute(self, inputs, outputs):
        rho_isa_kgm3 = 1.225
        outputs['fltcond_Utrue'] = inputs['fltcond_Ueas']*np.sqrt(rho_isa_kgm3/inputs['fltcond_rho'])
        
    def compute_partials(self, inputs, partials):
        rho_isa_kgm3 = 1.225
        partials['fltcond_Utrue', 'fltcond_Ueas'] = np.sqrt(rho_isa_kgm3/inputs['fltcond_rho'])
        partials['fltcond_Utrue', 'fltcond_rho'] = inputs['fltcond_Ueas']*np.sqrt(rho_isa_kgm3)*(-1/2)*inputs['fltcond_rho']**(-3/2)