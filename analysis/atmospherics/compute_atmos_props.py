from openConcept.analysis.atmospherics.temperature_comp import TemperatureComp
from openConcept.analysis.atmospherics.pressure_comp import PressureComp
from openConcept.analysis.atmospherics.density_comp import DensityComp
from openConcept.analysis.atmospherics.dynamic_pressure_comp import DynamicPressureComp
from openConcept.analysis.atmospherics.true_airspeed import TrueAirspeedComp

import numpy as np
from openmdao.api import ExplicitComponent, Group, Problem, IndepVarComp

class InputConverter(ExplicitComponent):
    """
    The differentiable standard atmosphere from Hwang and Jasa is unitless and modular. 
    This model adds a unitized interface to other higher-level model interfaces.
    """
    def initialize(self):
        self.options.declare('num_nodes', default=1, desc='Number of flight/control conditions')
    def setup(self):
        nn = self.options['num_nodes']
        self.add_input('fltcond_h', units='km', desc='Flight condition altitude',shape=(nn,))
        self.add_input('fltcond_Ueas', units='m/s', desc='Flight condition airspeed (equivalent)', shape=(nn,)) 

        #outputs and partials
        self.add_output('h_km', desc='Height in kilometers with no units',shape=(nn,))
        self.add_output('v_m_s', desc='Equivalent airspeed in m/s with no units',shape=(nn,))
        self.declare_partials('h_km','fltcond_h',rows=range(nn),cols=range(nn))    
        self.declare_partials('v_m_s','fltcond_Ueas',rows=range(nn),cols=range(nn))        
    def compute(self, inputs, outputs):
        outputs['h_km'] = inputs['fltcond_h']
        outputs['v_m_s'] = inputs['fltcond_Ueas']
    def compute_partials(self, inputs, J):
        nn = self.options['num_nodes']
        J['h_km','fltcond_h'] = np.ones(nn)
        J['v_m_s','fltcond_Ueas'] = np.ones(nn)

class OutputConverter(ExplicitComponent):
    """
    The differentiable standard atmosphere from Hwang and Jasa is unitless and modular. 
    This model adds a unitized interface to other higher-level model interfaces.
    """
    def initialize(self):
        self.options.declare('num_nodes', default=1, desc='Number of flight/control conditions')
    def setup(self):
        nn = self.options['num_nodes']
        self.add_input('p_MPa', desc='Flight condition pressures',shape=(nn,))
        self.add_input('T_1e2_K', desc='Flight condition temp',shape=(nn,))
        self.add_input('rho_kg_m3', desc='Flight condition density',shape=(nn,))
        #self.add_input('q_1e4_N_m2', desc='Flight condition dynamic pressure',shape=(nn,))

        #outputs and partials
        self.add_output('fltcond_p', units='Pa', desc='Flight condition pressure with units',shape=(nn,))
        self.add_output('fltcond_rho', units='kg * m**-3', desc='Flight condition density with units',shape=(nn,))
        self.add_output('fltcond_T', units='K', desc='Flight condition temp with units',shape=(nn,))
        #self.add_output('fltcond_q', units='Pa', desc='Flight condition dynamic pressure with units',shape=(nn,))

        self.declare_partials(['fltcond_p'],['p_MPa'],rows=range(nn),cols=range(nn))        
        self.declare_partials(['fltcond_rho'],['rho_kg_m3'],rows=range(nn),cols=range(nn))            
        self.declare_partials(['fltcond_T'],['T_1e2_K'],rows=range(nn),cols=range(nn))     
        #self.declare_partials(['fltcond_q'],['q_1e4_N_m2'],rows=range(nn),cols=range(nn))            
       
    
    def compute(self, inputs, outputs):
        outputs['fltcond_p'] = inputs['p_MPa'] * 1e6
        outputs['fltcond_rho'] = inputs['rho_kg_m3']
        outputs['fltcond_T'] = inputs['T_1e2_K'] * 100
        #outputs['fltcond_q'] = inputs['q_1e4_N_m2'] * 1e4
    def compute_partials(self, inputs, J):
        nn = self.options['num_nodes']
        J['fltcond_p','p_MPa'] = 1e6*np.ones(nn)
        J['fltcond_T','T_1e2_K'] = 100*np.ones(nn)
        J['fltcond_rho','rho_kg_m3'] = np.ones(nn)
        #J['fltcond_q','q_1e4_N_m2'] = 1e4*np.ones(nn)


class ComputeAtmosphericProperties(Group):
    """This computes pressure, temperature, and density for a given altitude at ISA condtions. Also true airspeed from equivalent ~ indicated airspeed
    """
    def initialize(self):
        self.options.declare('num_nodes',default=1,desc="Number of mission analysis points to run")

    def setup(self):
        nn = self.options['num_nodes']
        self.add_subsystem('inputconv', InputConverter(num_nodes=nn),promotes_inputs=['*'])
        self.add_subsystem('temp', TemperatureComp(num_nodes=nn))
        self.add_subsystem('pressure',PressureComp(num_nodes=nn))
        self.add_subsystem('density',DensityComp(num_nodes=nn))
        self.add_subsystem('outputconv',OutputConverter(num_nodes=nn),promotes_outputs=['*'])
        self.add_subsystem('trueairspeed',TrueAirspeedComp(num_nodes=nn),promotes_inputs=['*'],promotes_outputs=['*'])
        self.add_subsystem('dynamicpressure',DynamicPressureComp(num_nodes=nn),promotes_inputs=["*"],promotes_outputs=["*"])

        self.connect('inputconv.h_km','temp.h_km')
        self.connect('inputconv.h_km','pressure.h_km')
        self.connect('pressure.p_MPa','density.p_MPa')
        self.connect('temp.T_1e2_K','density.T_1e2_K')
        self.connect('pressure.p_MPa','outputconv.p_MPa')
        self.connect('temp.T_1e2_K','outputconv.T_1e2_K')
        self.connect('density.rho_kg_m3','outputconv.rho_kg_m3')



class TestModel(Group):
    def setup(self):
        dvs = self.add_subsystem('alts',IndepVarComp(),promotes_outputs=["*"])
        fltconds = self.add_subsystem('stdatm',ComputeAtmosphericProperties(num_nodes=10),promotes_inputs=["fltcond_*"])
        dvs.add_output('fltcond_h',np.linspace(0,28000,10),units='ft')
        dvs.add_output('fltcond_Ueas',np.ones(10)*150,units='kn')

# class TestUnitConv(ExplicitComponent):
#     def setup(self):
#         self.add_input('fltcond_Ueas',unit='m/s',desc='Equiv airspeed')
#         self.add_output('fltcond_Utrue',unit='m/s'desc='True airspeed')
#     def compute(self,inputs,outputs):
#         outputs['fltcond_Utrue'] = inputs['fltcond_Ueas']


def testfunc():
    prob = Problem()
    prob.model= TestModel()
    prob.setup()
    prob.run_model()
    print('Altitude: ' + str(prob['stdatm.inputconv.h_km']))
    print('Temp: ' + str(prob['stdatm.fltcond_T']))
    print('Pressure: ' + str(prob['stdatm.fltcond_p']))
    print('Density: ' + str(prob['stdatm.fltcond_rho']))
    print('TAS: ' + str(prob['stdatm.fltcond_Utrue']))
    print('Dynamic pressure:' + str(prob['stdatm.fltcond_q']))
    #prob.model.list_inputs()
    #prob.model.list_outputs()
    prob.check_partials(compact_print=True)

if __name__ == "__main__":
    from openConcept.analysis.atmospherics.compute_atmos_props import testfunc
    testfunc()
