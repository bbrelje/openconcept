from openmdao.api import Problem, Group, IndepVarComp, BalanceComp, DirectSolver, NewtonSolver, DenseJacobian, ScipyKrylov
import numpy as np
import scipy.sparse as sp
from openConcept.analysis.atmospherics.compute_atmos_props import ComputeAtmosphericProperties
from openConcept.simple_series_hybrid import SeriesHybridElectricPropulsionSystem
from openConcept.analysis.simpson_integration import simpson_integral, simpson_partials, simpson_integral_every_node, simpson_partials_every_node
from openmdao.api import ExplicitComponent, ImplicitComponent, BalanceComp, ArmijoGoldsteinLS, NonlinearBlockGS




class MissionFlightConditions(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")

    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        self.add_input('mission_vspeed_climb', val=1000, units='ft / s', desc='Vertical speed in the climb segment')
        self.add_input('mission_vspeed_desc', val=-500, units='ft / s', desc='Vertical speed in the descent segment (should be neg)')
        self.add_input('mission_eas_climb', val=180, units='kn', desc='Indicated airspeed during climb')
        self.add_input('mission_eas_cruise', val=250, units='kn', desc='Cruise airspeed (indicated)')
        self.add_input('mission_eas_desc', val=170, units='kn', desc='Descent airspeed (indicated)')
        self.add_input('mission_h_ground', val=0, units='ft',desc='Airport altitude')
        self.add_input('mission_h_cruise', val=28000, units='ft', desc='Cruise altitude')

        self.add_output('fltcond_Ueas', units='kn', desc='indicated airspeed at each timepoint',shape=(3*nn,))
        self.add_output('fltcond_h', units='ft', desc='altitude at each timepoint',shape=(3*nn,))
        self.add_output('fltcond_vspeed', units='ft / s', desc='vectorial representation of vertical speed',shape=(3*nn,))
        self.add_output('mission_time_to_climb', units ='s', desc='Time from ground level to cruise')
        self.add_output('mission_time_to_descend', units='s', desc='Time to descend to ground from cruise')
        self.add_output('mission_dt_climb',units='s', desc='Timestep in climb phase')
        self.add_output('mission_dt_desc', units='s', desc='Timestep in descent phase')

        #the climb speeds only have influence over their respective mission segments
        self.declare_partials(['fltcond_Ueas'],['mission_eas_climb'],rows=np.arange(0,nn),cols=np.ones(nn)*0,val=np.ones(nn))
        self.declare_partials(['fltcond_Ueas'],['mission_eas_cruise'],rows=np.arange(nn,2*nn),cols=np.ones(nn)*0,val=np.ones(nn))
        self.declare_partials(['fltcond_Ueas'],['mission_eas_desc'],rows=np.arange(2*nn,3*nn),cols=np.ones(nn)*0,val=np.ones(nn))
        hcruisepartials = np.concatenate([np.linspace(0.0,1.0,nn),np.ones(nn),np.linspace(1.0,0.0,nn)])
        hgroundpartials = np.concatenate([np.linspace(1.0,0.0,nn),np.linspace(0.0,1.0,nn)])
        #the influence of each parameter linearly varies from 0 to 1 and vice versa on climb and descent. The partials are different lengths on purpose - no influence of ground on the mid-mission points so no partial derivative
        self.declare_partials(['fltcond_h'],['mission_h_cruise'],rows=range(3*nn),cols=np.zeros(3*nn),val=hcruisepartials)
        self.declare_partials(['fltcond_h'],['mission_h_ground'],rows=np.concatenate([np.arange(0,nn),np.arange(2*nn,3*nn)]),cols=np.zeros(2*nn),val=hgroundpartials)
        self.declare_partials(['fltcond_vspeed'],['mission_vspeed_climb'],rows=range(nn),cols=np.zeros(nn),val=np.ones(nn))
        self.declare_partials(['fltcond_vspeed'],['mission_vspeed_desc'],rows=np.arange(2*nn,3*nn),cols=np.zeros(nn),val=np.ones(nn))
        self.declare_partials(['mission_time_to_climb'],['mission_h_ground','mission_h_cruise','mission_vspeed_climb'])
        self.declare_partials(['mission_time_to_descend'],['mission_h_ground','mission_h_cruise','mission_vspeed_desc'])
        self.declare_partials(['mission_dt_climb'],['mission_h_ground','mission_h_cruise','mission_vspeed_climb'])
        self.declare_partials(['mission_dt_desc'],['mission_h_ground','mission_h_cruise','mission_vspeed_desc'])



    def compute(self,inputs,outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = n_int_per_seg*2 + 1
        hvec_climb = np.linspace(inputs['mission_h_ground'],inputs['mission_h_cruise'],nn)
        hvec_desc = np.linspace(inputs['mission_h_cruise'],inputs['mission_h_ground'],nn)
        hvec_cruise = np.ones(nn)*inputs['mission_h_cruise']
        outputs['fltcond_h'] = np.concatenate([hvec_climb,hvec_cruise,hvec_desc])
        debug = np.concatenate([np.ones(nn)*inputs['mission_eas_climb'],np.ones(nn)*inputs['mission_eas_cruise'],np.ones(nn)*inputs['mission_eas_desc']])
        outputs['fltcond_Ueas'] = np.concatenate([np.ones(nn)*inputs['mission_eas_climb'],np.ones(nn)*inputs['mission_eas_cruise'],np.ones(nn)*inputs['mission_eas_desc']])
        outputs['fltcond_vspeed'] = np.concatenate([np.ones(nn)*inputs['mission_vspeed_climb'],np.ones(nn)*0.0,np.ones(nn)*inputs['mission_vspeed_desc']])
        outputs['mission_time_to_climb'] = (inputs['mission_h_cruise']-inputs['mission_h_ground'])/inputs['mission_vspeed_climb']
        outputs['mission_time_to_descend'] = (inputs['mission_h_ground']-inputs['mission_h_cruise'])/inputs['mission_vspeed_desc']
        outputs['mission_dt_climb'] = (inputs['mission_h_cruise']-inputs['mission_h_ground'])/inputs['mission_vspeed_climb']/(nn-1)
        outputs['mission_dt_desc'] =  (inputs['mission_h_ground']-inputs['mission_h_cruise'])/inputs['mission_vspeed_desc']/(nn-1)

    def compute_partials(self, inputs, J):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = n_int_per_seg*2 + 1
        J['mission_time_to_climb','mission_h_cruise'] = 1/inputs['mission_vspeed_climb']
        J['mission_time_to_climb','mission_h_ground'] = -1/inputs['mission_vspeed_climb']
        J['mission_time_to_climb','mission_vspeed_climb'] = -(inputs['mission_h_cruise']-inputs['mission_h_ground'])/(inputs['mission_vspeed_climb']**2)
        J['mission_time_to_descend','mission_h_cruise'] = -1/inputs['mission_vspeed_desc']
        J['mission_time_to_descend','mission_h_ground'] = 1/inputs['mission_vspeed_desc']
        J['mission_time_to_descend','mission_vspeed_desc'] = -(inputs['mission_h_ground']-inputs['mission_h_cruise'])/(inputs['mission_vspeed_desc']**2)
        J['mission_dt_climb','mission_h_cruise'] = 1/inputs['mission_vspeed_climb']/(nn-1)
        J['mission_dt_climb','mission_h_ground'] = -1/inputs['mission_vspeed_climb']/(nn-1)
        J['mission_dt_climb','mission_vspeed_climb'] = -(inputs['mission_h_cruise']-inputs['mission_h_ground'])/(inputs['mission_vspeed_climb']**2)/(nn-1)
        J['mission_dt_desc','mission_h_cruise'] = -1/inputs['mission_vspeed_desc']/(nn-1)
        J['mission_dt_desc','mission_h_ground'] = 1/inputs['mission_vspeed_desc']/(nn-1)
        J['mission_dt_desc','mission_vspeed_desc'] = -(inputs['mission_h_ground']-inputs['mission_h_cruise'])/(inputs['mission_vspeed_desc']**2)/(nn-1)

class ComputeGroundspeeds(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")

    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        self.add_input('fltcond_vspeed', units='ft/s',shape=(3*nn,))
        self.add_input('fltcond_Utrue', units='ft/s',shape=(3*nn,))
        self.add_output('mission_groundspeed', units='ft/s',shape=(3*nn,))
        self.add_output('fltcond_cosgamma', shape=(3*nn,), desc='Cosine of the flight path angle')
        self.add_output('fltcond_singamma', shape=(3*nn,), desc='sin of the flight path angle' )
        self.declare_partials(['mission_groundspeed','fltcond_cosgamma','fltcond_singamma'],['fltcond_vspeed','fltcond_Utrue'],rows=range(3*nn),cols=range(3*nn))

    def compute(self, inputs, outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        #compute the groundspeed on climb and desc
        groundspeed =  np.sqrt(inputs['fltcond_Utrue']**2-inputs['fltcond_vspeed']**2)
        outputs['mission_groundspeed'] = groundspeed
        outputs['fltcond_singamma'] = inputs['fltcond_vspeed'] / inputs['fltcond_Utrue']
        outputs['fltcond_cosgamma'] = groundspeed / inputs['fltcond_Utrue']

    def compute_partials(self, inputs, J):
        groundspeed =  np.sqrt(inputs['fltcond_Utrue']**2-inputs['fltcond_vspeed']**2)
        J['mission_groundspeed','fltcond_vspeed'] = (1/2) / np.sqrt(inputs['fltcond_Utrue']**2-inputs['fltcond_vspeed']**2) * (-2) * inputs['fltcond_vspeed']
        J['mission_groundspeed','fltcond_Utrue'] = (1/2) / np.sqrt(inputs['fltcond_Utrue']**2-inputs['fltcond_vspeed']**2) * 2 * inputs['fltcond_Utrue']
        J['fltcond_singamma','fltcond_vspeed'] = 1 / inputs['fltcond_Utrue']
        J['fltcond_singamma','fltcond_Utrue'] = - inputs['fltcond_vspeed'] / inputs['fltcond_Utrue'] ** 2
        J['fltcond_cosgamma','fltcond_vspeed'] = J['mission_groundspeed','fltcond_vspeed'] / inputs['fltcond_Utrue']
        J['fltcond_cosgamma','fltcond_Utrue'] = (J['mission_groundspeed','fltcond_Utrue'] * inputs['fltcond_Utrue'] - groundspeed) / inputs['fltcond_Utrue']**2

class ComputeClimbDescentRanges(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")
        
    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        self.add_input('mission_groundspeed', units='ft/s',shape=(3*nn,))
        self.add_input('mission_time_to_climb', units='s')
        self.add_input('mission_time_to_descend', units='s')
        self.add_output('mission_descent_range',units='ft')
        self.add_output('mission_climb_range',units='ft')
        self.declare_partials(['mission_climb_range'],['mission_groundspeed'],rows=np.ones(nn)*0,cols=range(nn))
        self.declare_partials(['mission_descent_range'],['mission_groundspeed'],rows=np.ones(nn)*0,cols=np.arange(2*nn,3*nn))
        self.declare_partials(['mission_climb_range'],['mission_time_to_climb'])
        self.declare_partials(['mission_descent_range'],['mission_time_to_descend'])

    def compute(self, inputs, outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)

        groundspeed = inputs['mission_groundspeed']
        #compute distance traveled during climb and desc using Simpson's rule
        dt_climb = inputs['mission_time_to_climb'] / (nn-1)
        dt_desc = inputs['mission_time_to_descend'] / (nn-1)
        simpsons_vec = np.ones(nn)
        simpsons_vec[1:nn-1:2] = 4
        simpsons_vec[2:nn-1:2] = 2
        outputs['mission_climb_range'] = np.sum(simpsons_vec*groundspeed[0:nn])*dt_climb/3
        outputs['mission_descent_range'] = np.sum(simpsons_vec*groundspeed[2*nn:3*nn])*dt_desc/3

    def compute_partials(self, inputs, J):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)

        groundspeed = inputs['mission_groundspeed']
        simpsons_vec = np.ones(nn)
        simpsons_vec[1:nn-1:2] = 4
        simpsons_vec[2:nn-1:2] = 2

        J['mission_climb_range','mission_time_to_climb'] = np.sum(simpsons_vec*groundspeed[0:nn])/3/(nn-1)
        J['mission_descent_range','mission_time_to_descend'] = np.sum(simpsons_vec*groundspeed[2*nn:3*nn])/3/(nn-1)
        J['mission_climb_range','mission_groundspeed'] = simpsons_vec * inputs['mission_time_to_climb'] / (nn-1) / 3
        J['mission_descent_range','mission_groundspeed'] = simpsons_vec * inputs['mission_time_to_descend'] / (nn-1) / 3


class ComputeMissionTimings(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
    
        self.add_input('mission_groundspeed',units='ft/s',shape=(3*nn,))
        self.add_input('mission_climb_range', units='ft')
        self.add_input('mission_descent_range', units='ft')
        self.add_input('mission_range',units='ft')
        self.add_output('mission_cruise_range',units='ft')
        self.add_output('mission_cruise_time',units='s')
        self.add_output('mission_dt_cruise',units="s")
        self.declare_partials(['mission_cruise_range'],['mission_climb_range'],val=-1.0)
        self.declare_partials(['mission_cruise_range'],['mission_descent_range'],val=-1.0)
        self.declare_partials(['mission_cruise_range'],['mission_range'],val=1.0)
        self.declare_partials(['mission_cruise_time'],['mission_groundspeed'],rows=np.zeros(nn),cols=np.arange(nn,2*nn))
        self.declare_partials(['mission_cruise_time'],['mission_climb_range','mission_descent_range','mission_range'])
        self.declare_partials(['mission_dt_cruise'],['mission_groundspeed'],rows=np.zeros(nn),cols=np.arange(nn,2*nn))
        self.declare_partials(['mission_dt_cruise'],['mission_climb_range','mission_descent_range','mission_range'])

    def compute(self, inputs, outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
    
        simpsons_vec = np.ones(nn)
        simpsons_vec[1:nn-1:2] = 4
        simpsons_vec[2:nn-1:2] = 2

        #compute the cruise distance
        r_cruise = inputs['mission_range'] - inputs['mission_climb_range'] - inputs['mission_descent_range']
        if r_cruise < 0:
            raise ValueError('Cruise calculated to be less than 0. Change climb and descent rates and airspeeds or increase range')
        dt_cruise = 3*r_cruise/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])
        t_cruise = dt_cruise*(nn-1)

        outputs['mission_cruise_time'] = t_cruise
        outputs['mission_cruise_range'] = r_cruise
        outputs['mission_dt_cruise'] = dt_cruise

    def compute_partials(self, inputs, J):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)

        simpsons_vec = np.ones(nn)
        simpsons_vec[1:nn-1:2] = 4
        simpsons_vec[2:nn-1:2] = 2

        #compute the cruise distance
        r_cruise = inputs['mission_range'] - inputs['mission_climb_range'] - inputs['mission_descent_range']
        J['mission_cruise_time','mission_groundspeed'] = -3*r_cruise/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])**2 * (nn-1) * (simpsons_vec)
        J['mission_cruise_time','mission_climb_range'] = -3/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])*(nn-1)
        J['mission_cruise_time','mission_descent_range'] = -3/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])*(nn-1)
        J['mission_cruise_time','mission_range'] = 3/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])*(nn-1)

        J['mission_dt_cruise','mission_groundspeed'] = -3*r_cruise/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])**2  * (simpsons_vec)
        J['mission_dt_cruise','mission_climb_range'] = -3/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])
        J['mission_dt_cruise','mission_descent_range'] = -3/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])
        J['mission_dt_cruise','mission_range'] = 3/np.sum(simpsons_vec*inputs['mission_groundspeed'][nn:2*nn])

class ComputeSegmentFuelBurns(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")


    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
    
        self.add_input('fuel_flow',units='kg/s',shape=(3*nn,))
        self.add_input('mission_dt_climb', units='s')
        self.add_input('mission_dt_desc', units='s')
        self.add_input('mission_dt_cruise', units='s')

        self.add_output('mission_segment_fuel',units='kg',shape=(3*(nn-1)))

        #use dummy inputs for dt and q, just want the shapes
        wrt_q, wrt_dt = simpson_partials_every_node(np.ones(3),np.ones(3*nn),n_segments=3,n_simpson_intervals_per_segment=n_int_per_seg)

        self.declare_partials(['mission_segment_fuel'],['fuel_flow'],rows=wrt_q[0],cols=wrt_q[1])
        self.declare_partials(['mission_segment_fuel'],['mission_dt_climb'],rows=wrt_dt[0][0],cols=wrt_dt[1][0])
        self.declare_partials(['mission_segment_fuel'],['mission_dt_cruise'],rows=wrt_dt[0][1],cols=wrt_dt[1][1])
        self.declare_partials(['mission_segment_fuel'],['mission_dt_desc'],rows=wrt_dt[0][2],cols=wrt_dt[1][2])

    def compute(self,inputs,outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1) 
        ff = inputs['fuel_flow']
        dts =  [inputs['mission_dt_climb'], inputs['mission_dt_cruise'],inputs['mission_dt_desc']]
        int_ff, delta_ff = simpson_integral_every_node(dts,ff,n_segments=3,n_simpson_intervals_per_segment=n_int_per_seg)

        outputs['mission_segment_fuel'] = delta_ff

    def compute_partials(self,inputs,J):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1) 
        ff = inputs['fuel_flow']
        dts =  [inputs['mission_dt_climb'], inputs['mission_dt_cruise'],inputs['mission_dt_desc']]

        wrt_q, wrt_dt = simpson_partials_every_node(dts,ff,n_segments=3,n_simpson_intervals_per_segment=n_int_per_seg)

        J['mission_segment_fuel','fuel_flow'] = wrt_q[2]
        J['mission_segment_fuel','mission_dt_climb'] =  wrt_dt[2][0]
        J['mission_segment_fuel','mission_dt_cruise'] = wrt_dt[2][1]
        J['mission_segment_fuel','mission_dt_desc'] = wrt_dt[2][2]

class ComputeSegmentWeights(ExplicitComponent):
    
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")
    
    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        self.add_input('mission_segment_fuel',units='kg',shape=(3*(nn-1),))
        self.add_input('mission_weight_takeoff',units='kg')
        self.add_output('mission_weight',units='kg',shape=(3*nn,))

        n_seg = 3
        jacmat = np.tril(np.ones((n_seg*(nn-1),n_seg*(nn-1))))
        jacmat = np.insert(jacmat,0,np.zeros(n_seg*(nn-1)),axis=0)
        for i in range(1,n_seg):
            duplicate_row = jacmat[nn*i-1,:]
            jacmat = np.insert(jacmat,nn*i,duplicate_row,axis=0)

        self.declare_partials(['mission_weight'],['mission_segment_fuel'],val=sp.csr_matrix(jacmat))
        self.declare_partials(['mission_weight'],['mission_weight_takeoff'],rows=range(3*nn),cols=np.zeros(3*nn),val=np.ones(3*nn))

    def compute(self,inputs,outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1) 
        #first endpoint needs to be the takeoff weight; insert a zero to make them the same length
        n_seg = 3
        segweights = np.insert(inputs['mission_segment_fuel'],0,0)
        weights = np.cumsum(segweights)
        for i in range(1,n_seg):
            duplicate_row = weights[i*nn-1]
            weights = np.insert(weights,i*nn,duplicate_row)
        outputs['mission_weight'] = np.ones(3*nn)*inputs['mission_weight_takeoff'] + weights

class ComputeSegmentCL(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")

    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        n_seg = 3
        arange = np.arange(0,n_seg*nn)
        self.add_input('mission_weight',units='kg', shape=(n_seg*nn,))
        self.add_input('fltcond_q',units='N * m**-2', shape=(n_seg*nn,))
        self.add_input('dv_Sref',units='m **2')
        self.add_input('fltcond_cosgamma', shape=(n_seg*nn,))
        self.add_output('fltcond_CL',shape=(n_seg*nn,))


        self.declare_partials(['fltcond_CL'],['mission_weight','fltcond_q',"fltcond_cosgamma"],rows=arange,cols=arange)
        self.declare_partials(['fltcond_CL'],['dv_Sref'],rows=arange,cols=np.zeros(n_seg*nn))

    def compute(self,inputs,outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1) 
        #first endpoint needs to be the takeoff weight; insert a zero to make them the same length
        n_seg = 3

        g = 9.80665 #m/s^2
        outputs['fltcond_CL'] = inputs['fltcond_cosgamma']*g*inputs['mission_weight']/inputs['fltcond_q']/inputs['dv_Sref']

    def compute_partials(self,inputs,J):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1) 
        #first endpoint needs to be the takeoff weight; insert a zero to make them the same length
        n_seg = 3

        g = 9.80665 #m/s^2
        J['fltcond_CL','mission_weight'] = inputs['fltcond_cosgamma']*g/inputs['fltcond_q']/inputs['dv_Sref']
        J['fltcond_CL','fltcond_q'] = - inputs['fltcond_cosgamma']*g*inputs['mission_weight'] / inputs['fltcond_q']**2 / inputs['dv_Sref']
        J['fltcond_CL','dv_Sref'] = - inputs['fltcond_cosgamma']*g*inputs['mission_weight'] / inputs['fltcond_q'] / inputs['dv_Sref']**2
        J['fltcond_CL','fltcond_cosgamma'] = g*inputs['mission_weight']/inputs['fltcond_q']/inputs['dv_Sref']

class PolarDrag(ExplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")

    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        n_seg = 3
        arange = np.arange(0,n_seg*nn)
        self.add_input('fltcond_CL', shape=(n_seg*nn,))
        self.add_input('fltcond_q',units='N * m**-2', shape=(n_seg*nn,))
        self.add_input('dv_Sref',units='m **2')
        self.add_input('aero_polar_CD0')
        self.add_input('aero_polar_e')
        self.add_input('dv_AR_wing')
        self.add_output('aero_drag',units='N',shape=(n_seg*nn,))


        self.declare_partials(['aero_drag'],['fltcond_CL','fltcond_q'],rows=arange,cols=arange)
        self.declare_partials(['aero_drag'],['dv_Sref','dv_AR_wing','aero_polar_CD0','aero_polar_e'],rows=arange,cols=np.zeros(n_seg*nn))

    def compute(self,inputs,outputs):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1) 
        #first endpoint needs to be the takeoff weight; insert a zero to make them the same length
        outputs['aero_drag'] = inputs['fltcond_q']*inputs['dv_Sref']*(inputs['aero_polar_CD0'] + inputs['fltcond_CL']**2 / np.pi / inputs['aero_polar_e'] / inputs['dv_AR_wing'])

    def compute_partials(self,inputs,J):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1) 
        #first endpoint needs to be the takeoff weight; insert a zero to make them the same length

        J['aero_drag','fltcond_q'] = inputs['dv_Sref']*(inputs['aero_polar_CD0'] + inputs['fltcond_CL']**2 / np.pi / inputs['aero_polar_e'] / inputs['dv_AR_wing'])
        J['aero_drag','fltcond_CL'] = inputs['fltcond_q']*inputs['dv_Sref']*(2 * inputs['fltcond_CL'] / np.pi / inputs['aero_polar_e'] / inputs['dv_AR_wing'])
        J['aero_drag','aero_polar_CD0'] = inputs['fltcond_q']*inputs['dv_Sref']
        J['aero_drag','aero_polar_e'] = - inputs['fltcond_q']*inputs['dv_Sref']*inputs['fltcond_CL']**2 / np.pi / inputs['aero_polar_e']**2 / inputs['dv_AR_wing']
        J['aero_drag','dv_Sref'] = inputs['fltcond_q']*(inputs['aero_polar_CD0'] + inputs['fltcond_CL']**2 / np.pi / inputs['aero_polar_e'] / inputs['dv_AR_wing'])
        J['aero_drag','dv_AR_wing'] = - inputs['fltcond_q']*inputs['dv_Sref'] * inputs['fltcond_CL']**2 / np.pi / inputs['aero_polar_e'] / inputs['dv_AR_wing']**2

class ThrustResidual(ImplicitComponent):
    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")
    
    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (n_int_per_seg*2 + 1)
        n_seg = 3
        arange = np.arange(0,n_seg*nn)
        self.add_input('aero_drag', units='N',shape=(n_seg*nn,))
        self.add_input('fltcond_singamma', shape=(n_seg*nn,))
        self.add_input('mission_weight',units='kg', shape=(n_seg*nn,))
        self.add_input('thrust', units='N', shape=(n_seg*nn,))
        self.add_input('power_A', units='W', shape=(n_seg*nn,))
        self.add_input('power_B', units='W', shape=(n_seg*nn,))
        self.add_output('throttle', shape=(n_seg*nn,))
        self.add_output('eng_throttle', shape=(n_seg*nn,))
        self.declare_partials(['throttle'],['aero_drag'],val=-sp.eye(nn*n_seg))
        self.declare_partials(['throttle'],['thrust'],val=sp.eye(nn*n_seg))
        self.declare_partials(['eng_throttle'],['power_A'],val=sp.eye(nn*n_seg))
        self.declare_partials(['eng_throttle'],['power_B'],val=-sp.eye(nn*n_seg))
        self.declare_partials(['throttle'], ['fltcond_singamma','mission_weight'],rows=arange,cols=arange)

    def apply_nonlinear(self, inputs, outputs, residuals):
        g = 9.80665 #m/s^2
        residuals['throttle'] = inputs['thrust'] - inputs['aero_drag'] - inputs['mission_weight']*g*inputs['fltcond_singamma']
        residuals['eng_throttle'] = inputs['power_A'] - inputs['power_B']
        print('Throttle: ' + str(outputs['throttle']))
        print('Eng: ' + str(outputs['eng_throttle']))
        print('Residuals:' +str(residuals['throttle']))
        print('Res2: ' + str(residuals['eng_throttle']))


    def linearize(self, inputs, outputs, partials):
        g = 9.80665 #m/s^2
        partials['throttle','mission_weight'] = -g*inputs['fltcond_singamma'] 
        partials['throttle','fltcond_singamma'] = -g*inputs['mission_weight']

class MissionAnalysis(Group):
    """This analysis group calculates thrust for a single engine configuration

    """

    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")
        self.options.declare('propmodel',desc='Propulsion model to use. Pass in the class, not an instance')

    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn = (2*n_int_per_seg+1)*3 #climb, cruise, descent
        #Create holders for control and flight condition parameters

        controls = self.add_subsystem('controls',IndepVarComp())
        conditions = self.add_subsystem('conds', MissionFlightConditions(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["mission_*"],promotes_outputs=["*"])
        atmos = self.add_subsystem('atmos',ComputeAtmosphericProperties(num_nodes=nn),promotes_inputs=["fltcond_h","fltcond_Ueas"],promotes_outputs=["fltcond_rho","fltcond_Utrue","fltcond_q"])
        groundspeeds = self.add_subsystem('gs',ComputeGroundspeeds(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["fltcond_*"],promotes_outputs=["mission_groundspeed","fltcond_*"])
        ranges = self.add_subsystem('ranges',ComputeClimbDescentRanges(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["mission_*"],promotes_outputs=["mission_*"])
        timings = self.add_subsystem('timings',ComputeMissionTimings(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["mission_*"],promotes_outputs=["mission_*"])
        #Introduce the propulsion model. Make sure to promote up flight condition and design variable inputs
        
        propmodelclass = self.options['propmodel']
        prop = self.add_subsystem('propmodel',propmodelclass(num_nodes=nn),promotes_inputs=["fltcond_*","dv_*"],promotes_outputs=["fuel_flow","thrust"])
        fbs = self.add_subsystem('fuelburn',ComputeSegmentFuelBurns(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["fuel_flow","mission_dt*"],promotes_outputs=["mission_segment_fuel"])
        wts = self.add_subsystem('weights',ComputeSegmentWeights(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["mission_segment_fuel","mission_weight_takeoff"],promotes_outputs=["mission_weight"])
        CLs = self.add_subsystem('CLs',ComputeSegmentCL(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["mission_weight","fltcond_q","dv_Sref"],promotes_outputs=["fltcond_CL"])
        drag = self.add_subsystem('drag',PolarDrag(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["fltcond_*","aero_*","dv_*"],promotes_outputs=["aero_drag"])
        td = self.add_subsystem('thrust',ThrustResidual(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["fltcond_singamma","aero_drag*","mission_weight*","thrust"])
        #control settings
        controls.add_output('prop1_rpm',np.ones(nn)*2500,units='rpm')
        controls.add_output('motor1_throttle',np.ones(nn)*1.0)
        controls.add_output('power_split_fraction',val=np.ones(nn)*0.5)

        #connect control settings to the various states in the propulsion model
        self.connect('controls.power_split_fraction','propmodel.hybrid_split.power_split_fraction')
        self.connect('thrust.throttle','propmodel.throttle')
        self.connect('thrust.eng_throttle','propmodel.eng1.throttle')
        self.connect('propmodel.hybrid_split.power_out_B','thrust.power_A')
        self.connect('propmodel.gen1.elec_power_out','thrust.power_B')
        self.connect('controls.prop1_rpm','propmodel.prop1.rpm')



class TestGroup(Group):
    def setup(self):
        dvs = self.add_subsystem('dvs',IndepVarComp(),promotes_outputs=["*"])
        mission = self.add_subsystem('missionanalysis',MissionAnalysis(num_integration_intervals_per_seg=2,propmodel=SeriesHybridElectricPropulsionSystem),promotes_inputs=["mission_vspeed*","mission_eas*","mission_h*","mission_range","mission_weight_takeoff","dv_*","aero_*"])
        dvs.add_output('mission_vspeed_climb',1000,units='ft/min')
        dvs.add_output('mission_vspeed_desc',-600,units='ft/min')
        dvs.add_output('mission_h_cruise',8,units='km')
        dvs.add_output('mission_h_ground',0,units='km')
        dvs.add_output('mission_eas_climb',180,units='kn')
        dvs.add_output('mission_eas_cruise',200,units='kn')
        dvs.add_output('mission_eas_desc',170,units='kn')
        dvs.add_output('mission_range',1000,units='km')
        dvs.add_output('mission_weight_takeoff',2000,units='kg')

        dvs.add_output('dv_prop1_diameter',3.0,units='m')
        dvs.add_output('dv_motor1_rating',1.0,units='MW')
        dvs.add_output('dv_gen1_rating',1.05,units='MW')
        dvs.add_output('dv_eng1_rating',1.1,units='MW')
        dvs.add_output('dv_batt1_weight',2000,units='kg')
        dvs.add_output('dv_Sref',193.0,units='ft**2')

        dvs.add_output('aero_polar_CD0',0.015)
        dvs.add_output('aero_polar_e',0.87)
        dvs.add_output('dv_AR_wing',13.5)




if __name__ == "__main__":

    from openConcept.analysis.mission_no_reserves import TestGroup
    import matplotlib.pyplot as plt
    prob = Problem()
    
    prob.model= TestGroup()
    prob.model.nonlinear_solver= NewtonSolver()
    prob.model.linear_solver = DirectSolver()
    prob.model.nonlinear_solver.options['solve_subsystems'] = True
    prob.model.nonlinear_solver.options['maxiter'] = 10
    prob.model.nonlinear_solver.options['atol'] = 1e-5
    prob.model.nonlinear_solver.options['rtol'] = 1e-5
    prob.model.nonlinear_solver.linesearch = ArmijoGoldsteinLS()
    prob.model.nonlinear_solver.linesearch.options['maxiter'] = 2
#    prob.model.nonlinear_solver.options['max_sub_solves'] = 1
    prob.setup()
    prob['missionanalysis.thrust.throttle'] = np.ones(15)
    prob['missionanalysis.thrust.eng_throttle'] = np.ones(15)

    prob.run_model()

    # # print "------Prop 1-------"
    # print('Thrust: ' + str(prob['missionanalysis.propmodel.prop1.thrust']))
    # plt.plot(prob['missionanalysis.propmodel.prop1.thrust'])
    # plt.show()

    # print('Weight: ' + str(prob['missionanalysis.propmodel.prop1.component_weight']))
    dtclimb = prob['missionanalysis.mission_dt_climb']
    dtcruise = prob['missionanalysis.mission_dt_cruise']
    dtdesc = prob['missionanalysis.mission_dt_desc']
    n_int = 2
    timevec = np.concatenate([np.linspace(0,2*n_int*dtclimb,2*n_int+1),np.linspace(2*n_int*dtclimb,2*n_int*dtclimb+2*n_int*dtcruise,2*n_int+1),np.linspace(2*n_int*(dtclimb+dtcruise),2*n_int*(dtclimb+dtcruise+dtdesc),2*n_int+1)])

    print('Flight conditions')
    plt.figure(1)
    plt.plot(timevec, prob['missionanalysis.conds.fltcond_Ueas'],'b.')
    plt.plot(timevec, prob['missionanalysis.atmos.trueairspeed.fltcond_Utrue'],'b-')
    plt.plot(timevec, prob['missionanalysis.gs.mission_groundspeed'],'g-')
    plt.title('Equivalent and true airspeed vs gs')

    print('Propulsion conditions')
    plt.figure(2)
    plt.plot(timevec, prob['missionanalysis.thrust'])
    plt.title('Thrust')

    plt.figure(3)
    plt.plot(timevec, prob['missionanalysis.fuel_flow'])
    plt.title('Fuel flow')

    plt.figure(4)
    # plt.plot(np.delete(timevec,[0,20,41]),np.cumsum(prob['missionanalysis.mission_segment_fuel']))
    plt.plot(timevec,prob['missionanalysis.mission_weight'])

    plt.figure(5)
    plt.plot(timevec,prob['missionanalysis.fltcond_CL'])

    plt.figure(6)
    plt.plot(timevec,prob['missionanalysis.aero_drag'])
    plt.show()

    print('Total fuel flow:' + str(np.sum(prob['missionanalysis.fuelburn.mission_segment_fuel'])))


    #prob.model.list_inputs()
    #prob.model.list_outputs()
    #prob.check_partials(compact_print=True)
    #prob.check_totals(compact_print=True)
