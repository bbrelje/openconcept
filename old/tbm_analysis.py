from openmdao.api import Problem, Group, IndepVarComp, DirectSolver, NewtonSolver, ScipyKrylov
from openmdao.api import ScipyOptimizeDriver, ExplicitComponent, ImplicitComponent, ArmijoGoldsteinLS

import numpy as np
from openConcept.analysis.atmospherics.compute_atmos_props import ComputeAtmosphericProperties
from openConcept.simple_turboprop import TurbopropPropulsionSystem
from openConcept.analysis.combo import Combiner, Splitter
from openConcept.analysis.aero import StallSpeed

from openConcept.analysis.takeoff import TakeoffFlightConditions, TakeoffTotalDistance
from openConcept.analysis.mission import MissionFlightConditions, MissionNoReserves

class TotalAnalysis(Group):
    """This analysis group calculates TOFL and mission fuel burn as well as many other quantities for an example airplane. Elements may be overridden or replaced as needed.
        Should be instantiated as the top-level model

    """

    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")
        #self.options.declare('propmodel',desc='Propulsion model to use. Pass in the class, not an instance')

    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn_tot_to = (2*n_int_per_seg+1)*3 +2 #v0v1,v1vr,v1v0, vtr, v2
        nn_tot_m = (2*n_int_per_seg+1)*3
        nn_tot=nn_tot_to+nn_tot_m
        nn = (2*n_int_per_seg+1)

        #Create holders for control and flight condition parameters. Add these as design variables as necessary for optimization when you define the prob

        dv_comp = self.add_subsystem('dv_comp',IndepVarComp(),promotes_outputs=["geom_*","takeoff_*","dv_*","mission_*"])
        #eventually replace the following with an analysis module
        dv_comp.add_output('aero_CLmax_flaps30',val=1.7)
        dv_comp.add_output('aero_polar_e',val=0.78)
        dv_comp.add_output('aero_polar_CD0_TO',val=0.03)
        dv_comp.add_output('aero_polar_CD0_cruise',0.019)

        #wing geometry variables
        dv_comp.add_output('geom_S_ref',val=18,units='m**2')
        dv_comp.add_output('geom_AR_wing',val=8.95)

        #design weights
        dv_comp.add_output('MTOW',val=3374,units='kg')

        #takeoff parameters
        dv_comp.add_output('takeoff_h',val=0,units='ft')
        dv_comp.add_output('takeoff_v1',val=85,units='kn')

        #mission parameters
        dv_comp.add_output('mission_vspeed_climb',1500,units='ft/min')
        dv_comp.add_output('mission_vspeed_desc',-600,units='ft/min')
        dv_comp.add_output('mission_h_cruise',28000,units='ft')
        dv_comp.add_output('mission_h_ground',0,units='km')
        dv_comp.add_output('mission_eas_climb',124,units='kn')
        dv_comp.add_output('mission_eas_cruise',201,units='kn')
        dv_comp.add_output('mission_eas_desc',140,units='kn')
        dv_comp.add_output('mission_range',1250,units='NM')
        dv_comp.add_output('mission_weight_takeoff',3374,units='kg')

        #propulsion parameters (rename this prefix at some point)
        dv_comp.add_output('dv_eng1_rating',850,units='hp')
        dv_comp.add_output('dv_prop1_diameter',2.31,units='m')




        #== Compute the stall speed (necessary for takeoff analysis)
        vstall = self.add_subsystem('vstall', StallSpeed(), promotes_inputs=["geom_S_ref"], promotes_outputs=["aero_Vstall_eas"])
        self.connect('dv_comp.aero_CLmax_flaps30','vstall.aero_CLmax_flapsdown')
        
        #==Calculate flight conditions for the takeoff and mission segments here
        mission_conditions = self.add_subsystem('mission_conditions', MissionFlightConditions(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["mission_*"],promotes_outputs=["mission_*","fltcond_*"])
        takeoff_conditions = self.add_subsystem('takeoff_conditions', TakeoffFlightConditions(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["takeoff_*","aero_*"],promotes_outputs=["fltcond_*","takeoff_*"])
        fltcond_combiner = self.add_subsystem('fltcond_combiner', Combiner(num_nodes=[nn_tot_to,nn_tot_m],input_names_lists=[['fltcond_h_takeoff','fltcond_h_mission'],['fltcond_Ueas_takeoff','fltcond_Ueas_mission']],units=['m','m/s'],output_names=['fltcond_h','fltcond_Ueas']),promotes_inputs=["*"],promotes_outputs=["*"])

        #TODO: orphaned outputs of the mission_conditions moduel
        # self.add_output('fltcond_vspeed', units='m / s', desc='vectorial representation of vertical speed',shape=(3*nn,))
        # self.add_output('mission_time_to_climb', units ='s', desc='Time from ground level to cruise')
        # self.add_output('mission_time_to_descend', units='s', desc='Time to descend to ground from cruise')
        # self.add_output('mission_dt_climb',units='s', desc='Timestep in climb phase')
        # self.add_output('mission_dt_desc', units='s', desc='Timestep in descent phase')
        

        #==Calculate atmospheric properties and true airspeeds for all mission segments
        atmos = self.add_subsystem('atmos',ComputeAtmosphericProperties(num_nodes=nn_tot),promotes_inputs=["fltcond_h","fltcond_Ueas"],promotes_outputs=["fltcond_rho","fltcond_Utrue","fltcond_q"])
                
        #==Define control settings for the propulsion system.
        # Recall that all flight points including takeoff roll are calculated all at once
        # The structure of the takeoff vector should be:
        #[ nn points (takeoff at full power, v0 to v1),
        #  nn points (takeoff at engine-out power (if applicable), v1 to vr),
        #  nn points (hard braking at zero power or even reverse, vr to v0),
        # !CAUTION! 1 point (transition at OEI power (if applicable), v_trans)
        # !CAUTION! 1 point (v2 climb at OEI power (if app), v2)
        # ]
        # The mission vector should be set implicitly using the solver (driving T = D residual to 0)
        controls = self.add_subsystem('controls',IndepVarComp())
        controls.add_output('prop1_rpm',val=np.ones(nn_tot)*2000,units='rpm')

        throttle_vec = np.concatenate([np.ones(nn),np.ones(nn)*1.0,np.zeros(nn),np.ones(2)*1.0])/1.1
        controls.add_output('motor1_throttle_takeoff',val=throttle_vec)
        #controls.add_output('motor1_throttle_mission',val=np.ones(nn_tot_m)*0.7)
        throttle_combiner = self.add_subsystem('throttle_combiner', Combiner(num_nodes=[nn_tot_to,nn_tot_m],input_names_lists=[['motor1_throttle_takeoff','motor1_throttle_mission']],output_names=['motor1_throttle']),promotes_outputs=["*"])
        self.connect('controls.motor1_throttle_takeoff','throttle_combiner.motor1_throttle_takeoff')


        #==Calculate engine thrusts and fuel flows. You will need to override this module to vary number of engines, prop architecture, etc
        # Your propulsion model must promote up a single variable called "thrust" and a single variable called "fuel_flow". You may need to sum these at a lower level in the prop model group
        # You will probably need to add more control parameters if you use multiple engines. You may also need to add implicit solver states if, e.g. turbogenerator power setting depends on motor power setting

        prop = self.add_subsystem('propmodel',TurbopropPropulsionSystem(num_nodes=nn_tot),promotes_inputs=["fltcond_*","dv_*"],promotes_outputs=["fuel_flow","thrust"])
        #connect control settings to the various states in the propulsion model
        self.connect('controls.prop1_rpm','propmodel.prop1.rpm')
        self.connect('motor1_throttle','propmodel.throttle')

        #now we have flight conditions and propulsion outputs for all flight conditions. Split into our individual analysis phases
        #== Leave this alone==#
        inputs_to_split = ['fltcond_q','fltcond_Utrue','fuel_flow','thrust']
        segments_to_split_into = ['takeoff','mission']
        units = ['N * m**-2','m/s','kg/s','N']
        nn_each_segment = [nn_tot_to,nn_tot_m]
        output_names_lists = []
        for input_name in inputs_to_split:
            output_names_list = []
            for segment in segments_to_split_into:
                output_names_list.append(input_name+'_'+segment)
            output_names_lists.append(output_names_list)
        splitter = self.add_subsystem('splitter',Splitter(num_nodes=nn_each_segment,units=units,input_names=inputs_to_split,output_names_lists=output_names_lists),promotes_inputs=["*"],promotes_outputs=["*"])
        
        #==This next module calculates balanced field length, if applicable. Your optimizer or solver MUST implicitly drive the abort distance and oei takeoff distances to the same value by varying v1
        
        takeoff = self.add_subsystem('takeoff',TakeoffTotalDistance(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=['geom_*','fltcond_*_takeoff','thrust_takeoff','fuel_flow_takeoff'])
        self.connect('dv_comp.aero_polar_CD0_TO','takeoff.drag.aero_polar_CD0')
        self.connect('dv_comp.aero_polar_e','takeoff.drag.aero_polar_e')
        self.connect('fltcond_q_takeoff','takeoff.drag.fltcond_q')
        self.connect('fltcond_q_takeoff','takeoff.lift.fltcond_q')
        self.connect('dv_comp.MTOW','vstall.weight')
        self.connect('dv_comp.MTOW','takeoff.TOW')
        self.connect('takeoff_v1','takeoff.v0v1_dist.upper_limit')
        self.connect('takeoff_v1','takeoff.v1vr_dist.lower_limit')
        self.connect('takeoff_vr','takeoff.v1vr_dist.upper_limit')
        self.connect('takeoff_v1','takeoff.v1v0_dist.lower_limit')
        self.connect('takeoff_v1','takeoff.reaction.takeoff_v1')
        mission = self.add_subsystem('mission',MissionNoReserves(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["geom_*","fltcond_*_mission","thrust_mission","fuel_flow_mission","mission_*"])
        #the implicit component in the mission module drives the mission throttle settings. this is very important
        self.connect('mission.thrust.throttle','throttle_combiner.motor1_throttle_mission')

        #connect the aero parameters
        self.connect('dv_comp.aero_polar_CD0_cruise','mission.aero_polar_CD0')
        self.connect('dv_comp.aero_polar_e','mission.aero_polar_e')


if __name__ == "__main__":
    prob = Problem()
    n_int_per_seg = 5    
    prob.model= TotalAnalysis(num_integration_intervals_per_seg=n_int_per_seg)
    nn_tot_m = 3*(n_int_per_seg*2+1)
    prob.model.nonlinear_solver= NewtonSolver()
    prob.model.linear_solver = DirectSolver()

    prob.model.nonlinear_solver.options['solve_subsystems'] = True
    prob.model.nonlinear_solver.options['maxiter'] = 10
    prob.model.nonlinear_solver.options['atol'] = 1e-4
    prob.model.nonlinear_solver.options['rtol'] = 1e-4
    prob.model.nonlinear_solver.linesearch = ArmijoGoldsteinLS()
    prob.model.nonlinear_solver.linesearch.options['maxiter'] = 2

    prob.setup()
    prob['mission.thrust.throttle'] = np.ones(nn_tot_m)*0.5
    prob.run_model()


    #prob.check_partials(compact_print=True)
    #prob.model.list_inputs()
    #prob.model.list_outputs()
    # print(prob['fltcond_Ueas'])
    # print(prob['fltcond_h'])
    # print(prob['fltcond_rho'])
    # print(prob['fuel_flow'])
    print('Stall speed'+str(prob['aero_Vstall_eas']))
    print('Rotate speed'+str(prob['takeoff_vr']))

    print('V0V1 dist: '+str(prob['takeoff.v0v1_dist.delta_quantity']))
    print('V1VR dist: '+str(prob['takeoff.v1vr_dist.delta_quantity']))
    print('Braking dist:'+str(prob['takeoff.v1v0_dist.delta_quantity']))
    print('Climb angle(rad):'+str(prob['takeoff.takeoff_climb_gamma']))
    print('h_trans:'+str(prob['takeoff.h_transition']))
    print('s_trans:'+str(prob['takeoff.s_transition']))
    print('s_climb:'+str(prob['takeoff.s_climb']))
    print('TO (continue):'+str(prob['takeoff.takeoff_distance']))
    print('TO (abort):'+str(prob['takeoff.takeoff_distance_abort']))
    print(prob['mission.mission_total_fuel'])
    
    prob.model.list_inputs(print_arrays=True)
    prob.model.list_outputs(print_arrays=True)
    #prob.check_totals(compact_print=True)


    # prob.driver = ScipyOptimizeDriver()
    # prob.model.add_design_var('mission_h_cruise', lower=1000, upper=30000)
    # prob.model.add_design_var('mission_vspeed_climb',lower=500,upper=3000)
    # prob.model.add_design_var('mission_eas_climb',lower=85,upper=300)
    # prob.model.add_design_var('mission_eas_cruise',lower=150,upper=300)
    # prob.model.add_constraint('thrust.throttle',upper=np.ones(15)*0.95)
    # prob.model.add_objective('mission_total_fuel')


    # #prob.run_driver()
    # print(prob['mission_h_cruise'])

    # # # print "------Prop 1-------"
    # # print('Thrust: ' + str(prob['propmodel.prop1.thrust']))
    # # plt.plot(prob['propmodel.prop1.thrust'])
    # # plt.show()

    # # print('Weight: ' + str(prob['propmodel.prop1.component_weight']))
    # dtclimb = prob['mission_dt_climb']
    # dtcruise = prob['mission_dt_cruise']
    # dtdesc = prob['mission_dt_desc']
    # n_int = 3
    # timevec = np.concatenate([np.linspace(0,2*n_int*dtclimb,2*n_int+1),np.linspace(2*n_int*dtclimb,2*n_int*dtclimb+2*n_int*dtcruise,2*n_int+1),np.linspace(2*n_int*(dtclimb+dtcruise),2*n_int*(dtclimb+dtcruise+dtdesc),2*n_int+1)])
    # plots = True
    # if plots:
    #     print('Flight conditions')
    #     plt.figure(1)
    #     plt.plot(timevec, prob['conds.fltcond_Ueas_mission'],'b.')
    #     plt.plot(timevec, prob['atmos.trueairspeed.fltcond_Utrue_mission'],'b-')
    #     plt.plot(timevec, prob['gs.mission_groundspeed'],'g-')
    #     plt.title('Equivalent and true airspeed vs gs')

    #     print('Propulsion conditions')
    #     plt.figure(2)
    #     plt.plot(timevec, prob['thrust'])
    #     plt.title('Thrust')

    #     plt.figure(3)
    #     plt.plot(timevec, prob['fuel_flow_mission'])
    #     plt.title('Fuel flow')

    #     plt.figure(4)
    #     # plt.plot(np.delete(timevec,[0,20,41]),np.cumsum(prob['mission_segment_fuel']))
    #     plt.plot(timevec,prob['mission_weight'])
    #     plt.title('Weight')

    #     plt.figure(5)
    #     plt.plot(timevec,prob['fltcond_CL_mission'])
    #     plt.title('CL')

    #     plt.figure(6)
    #     plt.plot(timevec,prob['aero_drag'])
    #     plt.title('Drag')

    #     plt.figure(7)
    #     plt.plot(timevec,prob['propmodel.eng1.shaft_power_out'])
    #     plt.title('Shaft power')
    #     plt.show()
    # print('Total fuel flow (totalizer):' + str(prob['mission_total_fuel']))
    # print('Total fuel flow:' + str(np.sum(prob['mission_segment_fuel'])))
    

    # #prob.model.list_inputs()
    # #prob.model.list_outputs()
    # #prob.check_partials(compact_print=True)
    # #prob.check_totals(compact_print=True)

