from openmdao.api import ExplicitComponent, IndepVarComp, Group, Problem
import numpy as np
from scipy import sparse as sp
from six import string_types, integer_types

class Combiner(ExplicitComponent):
    """Concatenates input vectors into output vectors. E.g. combine flight conditions from multiple tools into one
    output_names: any number of vectors (name them in the options). Use a string to name just one, 
        or an iterable (list, set, etc) for more than one
    input_names_lists: for one output, a list of input vectors. for multiple outputs, a list of lists of input vectors. The outer list should be 
        same length as the outputs list. The inner lists all need to be the same length, e.g. each output has to have 
        the same number of inputs. 

    num_nodes is a list of ints of the lengths of the input vectors. 
        It needs to be known a priori. Same length as inner list of inputs. All sets of inputs have identical lengths, e.g.
        the first input for each output has length nn1, second has nn2, etc

    units is a list of the units of each output. Should be same length as outputs

    options: num_nodes, input_names, output_names, units

    """

    def initialize(self):
        self.options.declare('num_nodes',default=[1,1], desc="List of lengths of input vectors")
        self.options.declare('input_names_lists', desc="List(s) of names of the input vectors")
        self.options.declare('units',default=None,  desc="Unit(s) of the inputs and outputs")
        self.options.declare('output_names', desc="Name(s) of output total quantity")


    def setup(self):
        nn_list = self.options['num_nodes']
        nn_tot = np.sum(nn_list)
        input_names_lists = self.options['input_names_lists']
        output_names = self.options['output_names']
        units = self.options['units']
        if isinstance(nn_list, integer_types):
            nn_list = [nn_list]
        if not isinstance(output_names, string_types):
            #multiple outputs, need to handle the default (singluar) options
            if units is None:
                units = [None for i in range(len(output_names))]
        if isinstance(output_names, string_types) and isinstance(input_names_lists[0], string_types):
            #only one output to concatenate
            input_names_lists = [input_names_lists]
            output_names = [output_names]
            units=[units]

        elif isinstance(input_names_lists[0], string_types) or isinstance(output_names, string_types):
            raise ValueError('There should be one output name per list of input names')
        
        if len(input_names_lists) != len(output_names) or len(output_names) != len(units):
            raise ValueError('The input lists need to all be the same length')
        if len(input_names_lists[0]) != len(nn_list):
            raise ValueError('The num_nodes list needs to match the number of inputs per output')

        for i, output_name in enumerate(output_names):
            self.add_output(output_name, units=units[i], shape=(nn_tot,))
            for j, input_name in enumerate(input_names_lists[i]):
                if j == 0:
                    start_idx = 0
                else:
                    start_idx = np.sum(nn_list[0:j])
                end_idx = np.sum(nn_list[0:j+1])
                arange = np.arange(start_idx,end_idx)
                self.add_input(input_name, units=units[i], shape=(nn_list[j],))
                self.declare_partials([output_name],[input_name],rows=arange,cols=np.arange(0,nn_list[j]),val=np.ones(nn_list[j]))

    def compute(self, inputs, outputs):
        nn_list = self.options['num_nodes']
        nn_tot = np.sum(nn_list)
        input_names_lists = self.options['input_names_lists']
        output_names = self.options['output_names']
        units = self.options['units']
        if not isinstance(output_names, string_types):
            #multiple outputs, need to handle the default (singluar) options
            if units is None:
                units = [None for i in range(len(output_names))]
        if isinstance(output_names, string_types) and isinstance(input_names_lists[0], string_types):
            #only one output to concatenate
            input_names_lists = [input_names_lists]
            output_names = [output_names]
            units=[units]

        for i, output_name in enumerate(output_names):
            temp = np.array([])
            for j, input_name in enumerate(input_names_lists[i]):
                temp = np.concatenate([temp,inputs[input_name]])
            outputs[output_name] = temp

class Splitter(ExplicitComponent):
    """Splits input vectors into output vectors. E.g. split flight conditions from one tool into multiple
    input_names: any number of vectors (name them in the options). Use a string to name just one, 
        or an iterable (list, set, etc) for more than one
    output_names_lists: for one input, a list of output vectors. for multiple inputs, a list of lists of output vectors. The outer list should be 
        same length as the inputs list. The inner lists all need to be the same length, e.g. each input has to have 
        the same number of outputs. 

    num_nodes is a list of ints of the lengths of the output vectors. 
        It needs to be known a priori. Same length as inner list of outputs. All sets of outputs have identical lengths, e.g.
        the first output for each input has length nn1, second has nn2, etc. The input (long) vector is assumed to contain
        the contents of the outputs in the order in which the outputs are named.

    units is a list of the units of each input. Should be same length as inputs

    """

    def initialize(self):
        self.options.declare('num_nodes',default=[1,1], desc="List of lengths of output vectors")
        self.options.declare('output_names_lists', desc="List(s) of names of the input vectors")
        self.options.declare('units',default=None,  desc="Unit(s) of the inputs and outputs")
        self.options.declare('input_names', desc="Name(s) of input (long) vectors")


    def setup(self):
        nn_list = self.options['num_nodes']
        nn_tot = np.sum(nn_list)
        output_names_lists = self.options['output_names_lists']
        input_names = self.options['input_names']
        units = self.options['units']
        if not isinstance(input_names, string_types):
            #multiple inputs, need to handle the default (singluar) options
            if units is None:
                units = [None for i in range(len(input_names))]
        if isinstance(input_names, string_types) and isinstance(output_names_lists[0], string_types):
            #only one input to concatenate
            output_names_lists = [output_names_lists]
            input_names = [input_names]
            units=[units]

        elif isinstance(output_names_lists[0], string_types) or isinstance(input_names, string_types):
            raise ValueError('There should be one input name per list of output names')
        
        if len(output_names_lists) != len(input_names) or len(input_names) != len(units):
            raise ValueError('The lists need to all be the same length')
        if len(output_names_lists[0]) != len(nn_list):
            raise ValueError('The num_nodes list needs to match the number of outputs per input')

        for i, input_name in enumerate(input_names):
            self.add_input(input_name, units=units[i], shape=(nn_tot,))
            for j, output_name in enumerate(output_names_lists[i]):
                if j == 0:
                    start_idx = 0
                else:
                    start_idx = np.sum(nn_list[0:j])
                end_idx = np.sum(nn_list[0:j+1])
                arange = np.arange(start_idx,end_idx)
                self.add_output(output_name, units=units[i], shape=(nn_list[j],))
                self.declare_partials([output_name],[input_name],cols=arange,rows=np.arange(0,nn_list[j]),val=np.ones(nn_list[j]))

    def compute(self, inputs, outputs):
        nn_list = self.options['num_nodes']
        nn_tot = np.sum(nn_list)
        output_names_lists = self.options['output_names_lists']
        input_names = self.options['input_names']
        units = self.options['units']
        if not isinstance(input_names, string_types):
            #multiple inputs, need to handle the default (singluar) options
            if units is None:
                units = [None for i in range(len(input_names))]
        if isinstance(input_names, string_types) and isinstance(output_names_lists[0], string_types):
            #only one input to concatenate
            output_names_lists = [output_names_lists]
            input_names = [input_names]
            units=[units]

        for i, input_name in enumerate(input_names):
            for j, output_name in enumerate(output_names_lists[i]):
                if j == 0:
                    start_idx = 0
                else:
                    start_idx = np.sum(nn_list[0:j])
                end_idx = np.sum(nn_list[0:j+1])
                outputs[output_name] = inputs[input_name][start_idx:end_idx]

class TestModel(Group):
    def setup(self):
        dvs = self.add_subsystem('dvs',IndepVarComp(),promotes_outputs=["*"])
        combine = self.add_subsystem('combiner',Combiner(num_nodes=[10,10],units=['kg','m'],output_names=['test_out_1','test_out_2'],input_names_lists=[['test_1','test_2'],['test_3','test_4']]),promotes_inputs=["*"],promotes_outputs=["*"])
        split = self.add_subsystem('splitter',Splitter(num_nodes=[10,10],units=['kg','m'],input_names=['test_out_1','test_out_2'],output_names_lists=[['test_1_o','test_2_o'],['test_3_o','test_4_o']]),promotes_inputs=["*"],promotes_outputs=["*"])
        dvs.add_output('test_1',np.linspace(0,1,10),units='kg')
        dvs.add_output('test_2',np.ones(10),units='kg')
        dvs.add_output('test_3',np.ones(10),units='m')
        dvs.add_output('test_4',np.random.normal(size=10),units='m')



if __name__ == "__main__":
    prob = Problem()
    
    prob.model= TestModel()
    prob.setup()
    prob.run_model()
    print('Combiner: ' + str(prob['test_out_1']))
    print('Combiner: ' + str(prob['test_out_2']))
    print('Splitter: ' + str(prob['test_1_o']))
    print('Splitter: ' + str(prob['test_2_o']))
    print('Splitter: ' + str(prob['test_3_o']))
    print('Splitter: ' + str(prob['test_4_o']))

    prob.check_partials(compact_print=True)

