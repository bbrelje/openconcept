from openmdao.api import ExplicitComponent, IndepVarComp, Group, Problem
import numpy as np
from scipy import sparse as sp
from six import string_types

class ElementMultiply(ExplicitComponent):
    """Multiply input vectors element-wise
    outputs: any number of vectors of length nn (name them in the options). Use a string to name just one, 
        or an iterable (list, set, etc) for more than one
    inputs: any number of nn-length vectors per output, to be added element-wise. Use a list/set to provide inputs to one output, 
        or a list of lists to provide inputs for multiple outputs
    num_nodes is same for all inputs
    options: num_nodes, input_names, output_names, units, scaling_factors
    """

    def initialize(self):
        self.options.declare('num_nodes',default=1, desc="Length of input vector")
        self.options.declare('input_names_lists', desc="List(s) of names of the input vectors")
        self.options.declare('units',default=None,  desc="Unit(s) of the inputs. A list of length (inputs) for one output, or a list of lists for multiple outputs")
        self.options.declare('output_names', desc="Name(s) of output total quantity")


    def setup(self):
        nn = self.options['num_nodes']
        input_names_lists = self.options['input_names_lists']
        output_names = self.options['output_names']
        units = self.options['units']

        if isinstance(output_names, string_types) and isinstance(input_names_lists[0], string_types):
            #only one input/output/sum to calculate
            input_names_lists = [input_names_lists]
            output_names = [output_names]
            units=[units]

        elif isinstance(input_names_lists[0], string_types) or isinstance(output_names, string_types):
            raise ValueError('There should be one output name per list of input names')
        
        if len(input_names_lists) != len(output_names) or len(output_names) != len(units):
            raise ValueError('The input lists need to all be the same length')

        arange = np.arange(0,nn)

        for i, output_name in enumerate(output_names):
            temp = ''
            for j, input_name in enumerate(input_names_lists[i]):
                if units is None:
                    self.add_input(input_name, units=None, shape=(nn,))
                else:
                    self.add_input(input_name, units=units[i][j], shape=(nn,))
                self.declare_partials([output_name],[input_name],rows=arange,cols=arange)
                temp = temp+'('+units[i][j]+')*'
            self.add_output(output_name, units=temp[:-1], shape=(nn,))


    def compute(self, inputs, outputs):
        nn = self.options['num_nodes']
        input_names_lists = self.options['input_names_lists']
        output_names = self.options['output_names']

        if isinstance(output_names, string_types) and isinstance(input_names_lists[0], string_types):
            #only one input/output/sum to calculate
            input_names_lists = [input_names_lists]
            output_names = [output_names]


        for i, output_name in enumerate(output_names):
            temp = inputs[input_names_lists[i][0]]
            for j, input_name in enumerate(input_names_lists[i][1:]):
                temp = temp * inputs[input_name]
            outputs[output_name] = temp

    def compute_partials(self, inputs, J):
        nn = self.options['num_nodes']
        input_names_lists = self.options['input_names_lists']
        output_names = self.options['output_names']
        if isinstance(output_names, string_types) and isinstance(input_names_lists[0], string_types):
            #only one input/output/sum to calculate
            input_names_lists = [input_names_lists]
            output_names = [output_names]

        for i, output_name in enumerate(output_names):
            for j, input_name in enumerate(input_names_lists[i]):
                temp = np.ones(nn)                
                for k, inp_2 in enumerate(input_names_lists[i]):
                    if inp_2 != input_name:
                        temp = temp*inputs[inp_2]
                J[output_name,input_name] = temp

class TestModel(Group):
    def setup(self):
        dvs = self.add_subsystem('dvs',IndepVarComp(),promotes_outputs=["*"])
        add = self.add_subsystem('multi',ElementMultiply(num_nodes=11,units=[['kg','m'],['kg','m']],output_names=['test_out_1','test_out_2'],input_names_lists=[['test_1','test_2'],['test_3','test_4']]),promotes_inputs=["*"])
        dvs.add_output('test_1',np.linspace(0,1,11),units='kg')
        dvs.add_output('test_2',np.linspace(1,0,11),units='m')
        dvs.add_output('test_3',np.ones(11),units='kg')
        dvs.add_output('test_4',np.ones(11),units='m')



if __name__ == "__main__":
    prob = Problem()
    
    prob.model= TestModel()
    prob.setup()
    prob.run_model()
    print('Multiplier: ' + str(prob['multi.test_out_1']))
    print('Multiplier: ' + str(prob['multi.test_out_2']))

    prob.check_partials(compact_print=True)

