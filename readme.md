# openConcept - A Conceptual Design tool with efficient gradients in OpenMDAO

- Working title subject to change
- Copyright Benjamin J Brelje 2018 all rights reserved

## Done so far
- Simple propulsion-related models, enough to do conventional aircraft, all-electric, or series-hybrid prop driven aircraft and eVTOLS
- Vectorization of the propulsion components for multipoint mission analysis
- Example: see simple_series_hybrid.py in the main folder. 

## To do
- Canned mission analysis routines (TOFL, cruise analysis)
- Basic weight and drag (enough to do a toy optimization)
- Populate the models with good default technology factors and cost data