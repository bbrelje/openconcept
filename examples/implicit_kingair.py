from openmdao.api import Problem, Group, IndepVarComp, DirectSolver, NewtonSolver, ScipyKrylov, BoundsEnforceLS
from openmdao.api import ScipyOptimizeDriver, ExplicitComponent, ImplicitComponent, ArmijoGoldsteinLS, BalanceComp
#-------These imports are generic and should be left alone
import numpy as np
from openconcept.analysis.atmospherics.compute_atmos_props import ComputeAtmosphericProperties
from openconcept.utilities.math import Combiner, Splitter, ElementMultiply
from openconcept.analysis.aerodynamics import StallSpeed
from openconcept.analysis.takeoff import TakeoffFlightConditions, TakeoffTotalDistance, ComputeBalancedFieldLengthResidual, BalancedFieldLengthResidual
from openconcept.analysis.mission import MissionFlightConditions, MissionNoReserves, ComputeDesignMissionResiduals

#These imports are particular to this airplane
from openconcept.empirical_data.weights_turboprop import SingleTurboPropEmptyWeight
from openconcept.examples.simple_turboprop import TwinTurbopropPropulsionSystem


class TotalAnalysis(Group):
    """This analysis group calculates TOFL and mission fuel burn as well as many other quantities for an example airplane. Elements may be overridden or replaced as needed.
        Should be instantiated as the top-level model
    """

    def initialize(self):
        self.options.declare('num_integration_intervals_per_seg',default=5,desc="Number of Simpson intervals to use per seg (eg. climb, cruise, descend). Number of time points is 2N+1")
        self.options.declare('use_newton',default=False)
    def setup(self):
        n_int_per_seg = self.options['num_integration_intervals_per_seg']
        nn_tot_to = (2*n_int_per_seg+1)*3 +2 #v0v1,v1vr,v1v0, vtr, v2
        nn_tot_m = (2*n_int_per_seg+1)*3
        nn_tot=nn_tot_to+nn_tot_m
        nn = (2*n_int_per_seg+1)
        use_newton = self.options['use_newton']

        #Define input variables
        dv_comp = self.add_subsystem('dv_comp',IndepVarComp(),promotes_outputs=["*"])
        #eventually replace the following aerodynamic parameters with an analysis module (maybe OpenAeroStruct)
        dv_comp.add_output('aero_CLmax_flaps30',val=1.3)
        dv_comp.add_output('aero_polar_e',val=0.80)
        dv_comp.add_output('aero_polar_CD0_TO',val=0.03)
        dv_comp.add_output('aero_polar_CD0_cruise',0.021)

        #Wing geometry variables
        dv_comp.add_output('geom_S_ref',val=27.308,units='m**2')
        dv_comp.add_output('geom_AR_wing',val=8.5834)
        dv_comp.add_output('geom_c4sweep_wing',1.0,units='deg')
        dv_comp.add_output('geom_taper_wing',0.397)
        dv_comp.add_output('geom_toverc_wing',0.19)
        dv_comp.add_output('geom_S_h',8.08,units='m**2')
        dv_comp.add_output('geom_S_v',3.40,units='m**2')
        dv_comp.add_output('geom_htailarm',5.33,units='m')
        dv_comp.add_output('geom_l_fuselage',10.79,units='m')
        dv_comp.add_output('geom_h_fuselage',1.9,units='m')
        dv_comp.add_output('geom_w_fuselage',1.6,units='m')
        dv_comp.add_output('geom_S_fuselage',41.3,units='m**2')
        dv_comp.add_output('geom_l_nosegear', 0.95, units='m')
        dv_comp.add_output('geom_l_maingear', 0.88, units='m')

        #Design weights
        dv_comp.add_output('MTOW',val=4581,units='kg')
        dv_comp.add_output('W_fuel_max',1166,units='kg')
        dv_comp.add_output('MLW', 4355, units='kg')

        #takeoff parameters
        dv_comp.add_output('takeoff_h',val=0,units='ft')
        if not use_newton:
            dv_comp.add_output('takeoff_v1',val=86,units='kn')

        #mission parameters
        dv_comp.add_output('mission_vspeed_climb',1500,units='ft/min')
        dv_comp.add_output('mission_vspeed_desc',-600,units='ft/min')
        dv_comp.add_output('mission_h_cruise',29000,units='ft')
        dv_comp.add_output('mission_h_ground',0,units='km')
        dv_comp.add_output('mission_eas_climb',124,units='kn')
        dv_comp.add_output('mission_eas_cruise',170,units='kn')
        dv_comp.add_output('mission_eas_desc',130,units='kn')
        dv_comp.add_output('mission_range',1000,units='NM')
        dv_comp.add_output('mission_design_payload',1300,units='lb')
        #Can reenable the following if you want to analyze a mission other than at MTOW
        #dv_comp.add_output('mission_weight_takeoff',3353,units='kg')

        #propulsion parameters (rename this prefix at some point)
        dv_comp.add_output('dv_eng_rating',750,units='hp')
        dv_comp.add_output('dv_prop_diameter',2.28,units='m')

        #Some additional parameters needed by the empirical weights tools
        dv_comp.add_output('num_passengers_max', 8)
        dv_comp.add_output('num_engines', 2)
        dv_comp.add_output('q_cruise', 98, units='lb * ft**-2')

        #== Compute the stall speed (necessary for takeoff analysis - leave this alone)
        vstall = self.add_subsystem('vstall', StallSpeed(), promotes_inputs=["geom_S_ref"], promotes_outputs=["aero_Vstall_eas"])
        self.connect('aero_CLmax_flaps30','vstall.aero_CLmax_flapsdown')
        
        #==Calculate flight conditions for the takeoff and mission segments here (leave this alone)
        mission_conditions = self.add_subsystem('mission_conditions', MissionFlightConditions(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["mission_*"],promotes_outputs=["mission_*","fltcond_*"])
        takeoff_conditions = self.add_subsystem('takeoff_conditions', TakeoffFlightConditions(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=["takeoff_*","aero_*"],promotes_outputs=["fltcond_*","takeoff_*"])
        fltcond_combiner = self.add_subsystem('fltcond_combiner', Combiner(num_nodes=[nn_tot_to,nn_tot_m],input_names_lists=[['fltcond_h_takeoff','fltcond_h_mission'],['fltcond_Ueas_takeoff','fltcond_Ueas_mission']],units=['m','m/s'],output_names=['fltcond_h','fltcond_Ueas']),promotes_inputs=["*"],promotes_outputs=["*"])

        #==Calculate atmospheric properties and true airspeeds for all mission segments
        atmos = self.add_subsystem('atmos',ComputeAtmosphericProperties(num_nodes=nn_tot),promotes_inputs=["fltcond_h","fltcond_Ueas"],promotes_outputs=["fltcond_rho","fltcond_Utrue","fltcond_q"])
                
        #==Define control settings for the propulsion system.
        # Recall that all flight points including takeoff roll are calculated all at once
        # The structure of the takeoff vector should be:
        #[ nn points (takeoff at full power, v0 to v1),
        #  nn points (takeoff at engine-out power (if applicable), v1 to vr),
        #  nn points (hard braking at zero power or even reverse, vr to v0),
        # !CAUTION! 1 point (transition at OEI power (if applicable), v_trans)
        # !CAUTION! 1 point (v2 climb at OEI power (if app), v2)
        # ]
        # The mission throttle vector should be set implicitly using the optimizer (driving T = D + sin(gamma)mg residual to 0)

        controls = self.add_subsystem('controls',IndepVarComp())
        #set the prop to 2000 rpm for all time
        controls.add_output('prop1_rpm',val=np.ones(nn_tot)*2000,units='rpm')
        controls.add_output('prop2_rpm',val=np.ones(nn_tot)*2000,units='rpm')

        #flat rated to 550hp from 750hp (derate 0.75)
        eng1_TO_throttle = np.concatenate([np.ones(nn),np.ones(nn),np.zeros(nn),np.ones(2)*1.0])*0.75
        #OEI during v1 to vref segment
        eng2_TO_throttle = np.concatenate([np.ones(nn),np.zeros(nn),np.zeros(nn),np.zeros(2)*1.0])*0.75
        controls.add_output('eng1_throttle_takeoff',val=eng1_TO_throttle)
        controls.add_output('eng2_throttle_takeoff',val=eng2_TO_throttle)
        #the throttle is set for the mission using the optimizer at a higher level script
        if not use_newton:
            controls.add_output('eng_throttle_mission',val=np.ones(nn_tot_m)*0.7)

        #combine the various controls together into one vector
        throttle_combiner = self.add_subsystem('throttle_combiner', Combiner(num_nodes=[nn_tot_to,nn_tot_m],input_names_lists=[['eng1_throttle_takeoff','eng1_throttle_mission'],['eng2_throttle_takeoff','eng2_throttle_mission']],output_names=['eng1_throttle','eng2_throttle']),promotes_outputs=["*"])
        self.connect('controls.eng1_throttle_takeoff','throttle_combiner.eng1_throttle_takeoff')
        self.connect('controls.eng2_throttle_takeoff','throttle_combiner.eng2_throttle_takeoff')
        if not use_newton:
            self.connect('controls.eng_throttle_mission','throttle_combiner.eng1_throttle_mission')
            self.connect('controls.eng_throttle_mission','throttle_combiner.eng2_throttle_mission')

        #==Calculate engine thrusts and fuel flows. You will need to override this module to vary number of engines, prop architecture, etc
        # Your propulsion model must promote up a single variable called "thrust" and a single variable called "fuel_flow". You may need to sum these at a lower level in the prop model group
        # You will probably need to add more control parameters if you use multiple engines. You may also need to add implicit solver states if, e.g. turbogenerator power setting depends on motor power setting

        prop = self.add_subsystem('propmodel',TwinTurbopropPropulsionSystem(num_nodes=nn_tot),promotes_inputs=["fltcond_*","dv_*"],promotes_outputs=["fuel_flow","thrust"])
        #connect control settings to the various states in the propulsion model
        self.connect('controls.prop1_rpm','propmodel.prop1.rpm')
        self.connect('controls.prop2_rpm','propmodel.prop2.rpm')
        self.connect('eng1_throttle','propmodel.eng1.throttle')
        self.connect('eng2_throttle','propmodel.eng2.throttle')


        #now we have flight conditions and propulsion outputs for all flight conditions. Split into our individual analysis phases
        #== Leave this alone==#
        inputs_to_split = ['fltcond_q','fltcond_Utrue','fuel_flow','thrust']
        segments_to_split_into = ['takeoff','mission']
        units = ['N * m**-2','m/s','kg/s','N']
        nn_each_segment = [nn_tot_to,nn_tot_m]
        output_names_lists = []
        for input_name in inputs_to_split:
            output_names_list = []
            for segment in segments_to_split_into:
                output_names_list.append(input_name+'_'+segment)
            output_names_lists.append(output_names_list)
        splitter = self.add_subsystem('splitter',Splitter(num_nodes=nn_each_segment,units=units,input_names=inputs_to_split,output_names_lists=output_names_lists),promotes_inputs=["*"],promotes_outputs=["*"])
        
        #==This next module calculates balanced field length, if applicable. Your optimizer or solver MUST implicitly drive the abort distance and oei takeoff distances to the same value by varying v1
        
        takeoff = self.add_subsystem('takeoff',TakeoffTotalDistance(num_integration_intervals_per_seg=n_int_per_seg),promotes_inputs=['geom_*','fltcond_*_takeoff','thrust_takeoff','fuel_flow_takeoff'])
        self.connect('aero_polar_CD0_TO','takeoff.drag.aero_polar_CD0')
        self.connect('aero_polar_e','takeoff.drag.aero_polar_e')
        self.connect('fltcond_q_takeoff','takeoff.drag.fltcond_q')
        self.connect('fltcond_q_takeoff','takeoff.lift.fltcond_q')
        self.connect('MTOW','vstall.weight')
        self.connect('MTOW','takeoff.TOW')
        if not use_newton:
            self.connect('takeoff_v1','takeoff.v0v1_dist.upper_limit')
            self.connect('takeoff_v1','takeoff.v1vr_dist.lower_limit')
            self.connect('takeoff_vr','takeoff.v1vr_dist.upper_limit')
            self.connect('takeoff_v1','takeoff.v1v0_dist.lower_limit')
            self.connect('takeoff_v1','takeoff.reaction.takeoff_v1')

        #==This module computes fuel consumption during the entire mission
        mission = self.add_subsystem('mission',MissionNoReserves(num_integration_intervals_per_seg=n_int_per_seg,use_newton=use_newton),promotes_inputs=["geom_*","fltcond_*_mission","thrust_mission","fuel_flow_mission","mission_*"])
        #remember that you will need to set the mission throttle implicitly using the optimizer/solver. This was done above when we mashed the control vectors all together.
        self.connect('MTOW','mission_weight_takeoff')
        #connect the aero parameters
        self.connect('aero_polar_CD0_cruise','mission.aero_polar_CD0')
        self.connect('aero_polar_e','mission.aero_polar_e')
        if use_newton:
            self.connect('mission.thrust.throttle','throttle_combiner.eng1_throttle_mission')
            self.connect('mission.thrust.throttle','throttle_combiner.eng2_throttle_mission')


        #==This module is an empirical weight tool specific to a single-engine turboprop airplane. You will need to modify or replace it.
        self.add_subsystem('OEW',SingleTurboPropEmptyWeight(),promotes_inputs=["*"])
        self.connect('dv_eng_rating','P_TO')
        #Don't forget to couple the propulsion system to the weights module like so:
        self.connect('propmodel.propellers_weight','W_propeller')
        self.connect('propmodel.engines_weight','W_engine')

        #==Finally, we need to compute certain quantities to ensure the airplane is feasible. Compute whether enough fuel volume exists, and whether the airplane burned more fuel than it can carry
        fuelmargins = self.add_subsystem('fuelmargins',ComputeDesignMissionResiduals(),promotes_inputs=["MTOW","mission_*","W_fuel_max"])
        self.connect('OEW.OEW','fuelmargins.OEW')
        self.connect('mission.mission_total_fuel','mission_total_fuel')

        #==Calculate the difference between the one-engine-out abort distance and one-engine-out takeoff distance with obstacle clearance
        if not use_newton:
            bflmargins = self.add_subsystem('bflmargins',ComputeBalancedFieldLengthResidual())
            
        else:
            bflmargins = self.add_subsystem('bflmargins',BalancedFieldLengthResidual(),promotes_outputs=["takeoff_v1"])
            self.connect('takeoff_v1','takeoff.v0v1_dist.upper_limit')
            self.connect('takeoff_v1','takeoff.v1vr_dist.lower_limit')
            self.connect('takeoff_vr','takeoff.v1vr_dist.upper_limit')
            self.connect('takeoff_v1','takeoff.v1v0_dist.lower_limit')
            self.connect('takeoff_v1','takeoff.reaction.takeoff_v1')


        self.connect('takeoff.takeoff_distance','bflmargins.takeoff_distance')
        self.connect('takeoff.takeoff_distance_abort','bflmargins.takeoff_distance_abort')

        #==That's it! Instantiate this model using a script and define your optimizer settings.


def define_analysis(n_int_per_seg,use_newton=False):
    """
    This function sets up the problem with all DVs and constraints necessary to perform analysis only (drives throttle residuals and BFL residuals to zero).
    This does NOT ensure that the airplane has enough fuel capacity or gross weight to fly the mission.
    """
    prob = Problem()
    prob.model= TotalAnalysis(num_integration_intervals_per_seg=n_int_per_seg,use_newton=use_newton)
    nn = n_int_per_seg*2+1
    nn_tot_m = 3*(n_int_per_seg*2+1)
    nn_tot_to = 3*(n_int_per_seg*2+1)+2
    nn_tot = 6*(n_int_per_seg*2+1)+2

    if use_newton:
        prob.model.nonlinear_solver= NewtonSolver()
        prob.model.linear_solver = ScipyKrylov()
        
        prob.model.nonlinear_solver.options['solve_subsystems'] = True
        prob.model.nonlinear_solver.options['maxiter'] = 10
        prob.model.nonlinear_solver.options['atol'] = 1e-5
        prob.model.nonlinear_solver.options['rtol'] = 1e-5
        prob.model.nonlinear_solver.linesearch = ArmijoGoldsteinLS()
        prob.model.nonlinear_solver.linesearch.options['maxiter'] = 10

    prob.driver = ScipyOptimizeDriver()
    #Required analysis constraints
    if not use_newton:
        prob.model.add_design_var('takeoff_v1',lower=50,upper=120)
        prob.model.add_design_var('controls.eng_throttle_mission',lower=np.zeros(nn_tot_m),upper=np.ones(nn_tot_m)*0.75)
        prob.model.add_constraint('mission.thrust_resid.thrust_residual',equals=np.zeros(nn_tot_m))
        prob.model.add_constraint('bflmargins.BFL_residual',equals=0.0)
    #prob.model.add_constraint('takeoff._rate_to_integrate_v0v1',lower=np.zeros(nn))
    #prob.model.add_constraint('takeoff._rate_to_integrate_v1vr',lower=np.zeros(nn))
    #prob.model.add_constraint('takeoff._rate_to_integrate_v1v0',upper=np.zeros(nn))
    return prob, nn_tot, nn_tot_m, nn_tot_to

def takeoff_check(prob):
    """
    In some cases, the numeric integration scheme used to calculate TOFL can give a spurious result if the airplane can't accelerate through to V1. This function 
    detects this case and raises an error. It should be called following every model.run_driver or run_model call.
    """
    v0v1 = prob['takeoff._rate_to_integrate_v0v1']
    v1vr = prob['takeoff._rate_to_integrate_v1vr']
    v1v0 = prob['takeoff._rate_to_integrate_v1v0']
    if np.sum(v0v1 < 0) > 0:
        raise ValueError('The aircraft was unable to reach v1 speed at the optimized design point. Restrict the design variables to add power or re-enable takeoff constraints')
    if np.sum(v1vr < 0) > 0:
        raise ValueError('The aircraft was unable to accelerate to vr from v1 (try adding power), or the v1 speed is higher than vr')
    if np.sum(v1v0 > 0) < 0:
        raise ValueError('Unusually, the aircraft continues to accelerate even after heavy braking in the abort phase of takeoff. Check your engine-out abort throttle settings (should be near zero thrust)')

if __name__ == "__main__":
    n_int_per_seg = 2    
    use_newton=True
    prob, nn_tot, nn_tot_m, nn_tot_to = define_analysis(n_int_per_seg,use_newton=use_newton)

    run_type = 'max_range'
    if run_type == 'optimization':
        print('======Performing Multidisciplinary Design Optimization===========')
        prob.model.add_design_var('MTOW', lower=3900, upper=5500)
        prob.model.add_design_var('geom_S_ref',lower=9,upper=40)
        prob.model.add_design_var('dv_eng_rating',lower=400,upper=1400)
        #prob.model.add_design_var('dv_prop_diameter',lower=1.6,upper=2.5)
        prob.model.add_design_var('W_fuel_max',lower=500,upper=3000)
        prob.model.add_constraint('fuelmargins.mission_MTOW_margin',equals=0.0)
        prob.model.add_constraint('fuelmargins.mission_fuel_capacity_margin',equals=0.0)
        prob.model.add_constraint('takeoff.takeoff_distance',upper=1348.3)
        prob.model.add_constraint('aero_Vstall_eas',upper=44.0)
        # prob.model.add_design_var('mission_eas_climb',lower=85,upper=300)
        # prob.model.add_design_var('mission_eas_cruise',lower=150,upper=300)
    elif run_type == 'max_range':
        print('======Analyzing Design Range at Given MTOW===========')
        prob.model.add_design_var('mission_range',lower=1000,upper=2500)
        prob.model.add_constraint('fuelmargins.mission_MTOW_margin',equals=0.0)
    else:
        print('======Analyzing Fuel Burn for Given Mision============')

    prob.model.add_objective('mission.mission_total_fuel')
    prob.setup(mode='fwd')
    prob['OEW.const.structural_fudge'] = 2.0
    if not use_newton:
        prob['controls.eng_throttle_mission'] = np.ones(nn_tot_m)*0.5
    else:
        prob['mission.thrust.throttle'] = np.ones(nn_tot_m)*0.5
        prob['bflmargins.takeoff_v1'] = 45
    prob.run_driver()
    takeoff_check(prob)
    prob.check_partials(compact_print=True)
    print('Design range: '+str(prob['mission_range']))
    print('OEW: '+str(prob['OEW.OEW']))
    print('Stall speed'+str(prob['aero_Vstall_eas']))
    print('Rotate speed'+str(prob['takeoff_vr']))
    # print('V0V1 dist: '+str(prob['takeoff.v0v1_dist.delta_quantity']))
    # print('V1VR dist: '+str(prob['takeoff.v1vr_dist.delta_quantity']))
    # print('Braking dist:'+str(prob['takeoff.v1v0_dist.delta_quantity']))
    print('Climb angle(rad):'+str(prob['takeoff.takeoff_climb_gamma']))
    # print('h_trans:'+str(prob['takeoff.h_transition']))
    # print('s_trans:'+str(prob['takeoff.s_transition']))
    # print('s_climb:'+str(prob['takeoff.s_climb']))
    print('TO (continue):'+str(prob['takeoff.takeoff_distance']))
    print('TO (abort):'+str(prob['takeoff.takeoff_distance_abort']))
    print('Fuel burn: '+ str(prob['mission.mission_total_fuel']))
    print('MTOW margin: '+str(prob['fuelmargins.mission_MTOW_margin']))
    print('S_ref: ' +str(prob['geom_S_ref']))
    print('Prop diam: '+str(prob['dv_prop_diameter']))
    print('MTOW: '+str(prob['MTOW']))
    print('Fuel cap:'+str(prob['W_fuel_max']))
    print('Rated power:'+str(prob['dv_eng_rating']))
    if not use_newton:
        print('Mission throttle settings:'+str(prob['controls.eng_throttle_mission']))
    else:
        print('Mission throttle settings:'+str(prob['mission.thrust.throttle']))
    



    #prob.model.list_inputs(print_arrays=True)
    #prob.model.list_outputs(print_arrays=True)

    # print(prob['mission_h_cruise'])

    # # # print "------Prop 1-------"
    # # print('Thrust: ' + str(prob['propmodel.prop1.thrust']))
    # # plt.plot(prob['propmodel.prop1.thrust'])
    # # plt.show()

    # # print('Weight: ' + str(prob['propmodel.prop1.component_weight']))
    # dtclimb = prob['mission_dt_climb']
    # dtcruise = prob['mission_dt_cruise']
    # dtdesc = prob['mission_dt_desc']
    # n_int = 3
    # timevec = np.concatenate([np.linspace(0,2*n_int*dtclimb,2*n_int+1),np.linspace(2*n_int*dtclimb,2*n_int*dtclimb+2*n_int*dtcruise,2*n_int+1),np.linspace(2*n_int*(dtclimb+dtcruise),2*n_int*(dtclimb+dtcruise+dtdesc),2*n_int+1)])
    # plots = True
    # if plots:
    #     print('Flight conditions')
    #     plt.figure(1)
    #     plt.plot(timevec, prob['conds.fltcond_Ueas_mission'],'b.')
    #     plt.plot(timevec, prob['atmos.trueairspeed.fltcond_Utrue_mission'],'b-')
    #     plt.plot(timevec, prob['gs.mission_groundspeed'],'g-')
    #     plt.title('Equivalent and true airspeed vs gs')

    #     print('Propulsion conditions')
    #     plt.figure(2)
    #     plt.plot(timevec, prob['thrust'])
    #     plt.title('Thrust')

    #     plt.figure(3)
    #     plt.plot(timevec, prob['fuel_flow_mission'])
    #     plt.title('Fuel flow')

    #     plt.figure(4)
    #     # plt.plot(np.delete(timevec,[0,20,41]),np.cumsum(prob['mission_segment_fuel']))
    #     plt.plot(timevec,prob['mission_weight'])
    #     plt.title('Weight')

    #     plt.figure(5)
    #     plt.plot(timevec,prob['fltcond_CL_mission'])
    #     plt.title('CL')

    #     plt.figure(6)
    #     plt.plot(timevec,prob['aero_drag'])
    #     plt.title('Drag')

    #     plt.figure(7)
    #     plt.plot(timevec,prob['propmodel.eng1.shaft_power_out'])
    #     plt.title('Shaft power')
    #     plt.show()
    # print('Total fuel flow (totalizer):' + str(prob['mission_total_fuel']))
    # print('Total fuel flow:' + str(np.sum(prob['mission_segment_fuel'])))
    

    # #prob.model.list_inputs()
    # #prob.model.list_outputs()
    # #prob.check_partials(compact_print=True)
    # #prob.check_totals(compact_print=True)

